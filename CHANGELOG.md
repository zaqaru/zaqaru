# Changelog

## [0.2] - In development

## [0.1] - 2020-11-11

### Added
- Initial version with hardcoded test level
- Implement player mechanics on various elements
- Implement antagonist behavior
- Import/export levels from/to JSON files
- Read from DAT and SCL levels
