/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNIT_HPP
#define UNIT_HPP

#include "box.hpp"
#include "defs.hpp"
#include "element.hpp"

class Scene;

class Unit
{
public:
	enum Request {
		NONE, STOP, GO_UP, GO_DOWN,
		GO_XN, GO_ZP, GO_ZN, GO_XP,
		ACT_XN, ACT_ZP, ACT_ZN, ACT_XP,
	};

	Unit(Scene *scene, const Vec3 &absolute_position);
	virtual ~Unit();

	bool check_stop_down(double dy = 0.0) const;
	bool check_stop_up() const;

	Vec3 coordinates() const;
	virtual void update(gametime dt, const std::vector<Unit *> &obstacles);

	virtual bool trappable() const = 0;

	Box get_aabb() const;
	Element *get_base() const { return base; }
	Vec3 get_rpos() const { return position; }

protected:
	enum State { NORMAL, TRAP_IN, TRAP_DOWN, TRAP_OUT };

	void autocenter_x(gametime dt);
	void autocenter_y(gametime dt);
	void autocenter_z(gametime dt);

	bool check_fall(gametime dt);
	bool check_field(gametime dt);
	void change_base(Element *new_base);
	virtual void check_object_presence() = 0;
	bool check_collisions(const std::vector<Unit *> &obstacles);
	bool check_horizontal_move(gametime dt, const std::vector<Unit *> &);
	bool check_vertical_move(gametime dt, const std::vector<Unit *> &obstacles);
	bool check_stop(double delta, Axis axis, Direction direction);
	void check_side_limits(Axis axis, Direction direction);
	bool check_sliding(gametime dt);
	void check_top_limit();
	void check_bottom_limit();
	virtual bool check_action() = 0;
	void exit_trap(gametime);
	virtual void set_intent(Request = NONE) = 0;

	Vec3 position;
	Request intent = NONE;
	State state = NORMAL;

	Scene *scene = nullptr;
	Element *base = nullptr;

	gametime trap_time = 0;
	double walk_speed;
	double climb_speed;
	double fall_speed;

private:
	void transfer_to_lower_element();
};

#endif // UNIT_HPP
