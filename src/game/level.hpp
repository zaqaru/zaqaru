/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEVEL_HPP
#define LEVEL_HPP

#include <fstream>
#include <map>
#include <set>
#include <string>

#include "vec.hpp"

class Scene;

/**
 * Level for import from 2D games
 */
class Level
{
public:
	Level(const std::string &filename, int number = 1);
	~Level();

	void initialize();
	void load(Scene &, int level = 1);
	std::string name() const;
	int size() const;

private:
	/** 2D level file formats */
	enum Type {
		UNKNOWN, SCL, DAT
	};

	/** 2D tile types (for most, as defined in Scavenger) */
	enum Tile {
		EMPTY = 0, BRICK, CEMENT, LADDER, RAIL, FAKE, ESCAPE,
		GOLD, ENEMY, HERO, HIDDEN, FLASHING,
		RAILX = 0x20, RESPAWN,
	};

	static const std::set<Level::Tile> STRUCTURAL_ELEMENTS;

	void add_objects(Scene &scene);
	void add_structure(Scene &scene);
	void add_respawn_spots();
	void build_scene(Scene &scene);
	void fill_additional_rows(Scene &scene, bool top, bool bottom);
	void fix_rails();
	void init_dat();
	void init_scl();
	void load_dat(int level);
	void load_scl(int level);
	void store_element(Tile, int, int);

	static unsigned long read_unsigned_long(std::ifstream &in);

	std::string filename;
	std::string level_name;
	std::ifstream input;

	std::set<Tile> unknown_ids;
	std::map<Vec2i, Tile> cells;

	bool initialized;
	Type level_type = UNKNOWN;
	int level_number;
	int total_levels = 0;
	int xmin = 0;
	int xmax = 0;
	int ymin = 0;
	int ymax = 0;
};

#endif // LEVEL_HPP
