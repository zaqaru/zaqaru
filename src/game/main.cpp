/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core.hpp"
#include "defs.hpp"
#include "helper.hpp"
#include "level.hpp"
#include "renderer.hpp"
#include "scene.hpp"
#include "settings.hpp"

#include <QCommandLineParser>
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <SDL2/SDL.h>

#include <iostream>
#include <sstream>

static void initialize_sdl();
static void parse_command_line(QCoreApplication &);
static void run_game();

/**
 * Entry point
 */
int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	QCoreApplication::setApplicationName(PROJECT_NAME);
	QCoreApplication::setApplicationVersion(PROJECT_VERSION);

	parse_command_line(app);

	try {
		initialize_sdl();
		run_game();
		Settings::save();
	}
	catch (const std::exception &e) {
		qCritical() << "Exception:" <<  e.what();
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
 * Initialize SDL
 */
void initialize_sdl()
{
	int status = SDL_Init(SDL_INIT_VIDEO);

	if (status < 0) {
		std::stringstream s;
		s << "SDL initialization failed." << std::endl;
		s << SDL_GetError();
		throw std::runtime_error(s.str());
	}
	std::atexit(SDL_Quit);
}

/**
 * Run game
 */
void run_game()
{
	Scene scene;
	Renderer renderer(scene);

	bool exit_program = false;

	renderer.initialize();

	bool internal_level = true;
	bool legacy_level = false;
	int nb_levels = 1;
	int current_level = 1;

	auto filename = Settings::read_level_file();
	current_level = std::max(1, Settings::read_level_number());

	Level level(filename);

	if (filename.size() > 0) {
		internal_level = false;
		if (!helper::string_ends_with(filename, ".json")) {
			legacy_level = true;
			level.initialize();
			nb_levels = level.size();
			current_level = std::min(current_level, nb_levels);
		}
	}

	while (!exit_program) {
		if (internal_level) {
			qDebug() << "Load internal level";
			scene.setup_test_level();
		} else if (legacy_level) {
			qDebug() << "Load legacy level";
			level.load(scene, current_level);
		} else {
			qDebug() << "Load JSON level";
			QFile resource(filename.c_str());
			if (!resource.open(QIODevice::ReadOnly))
			    throw std::runtime_error("Invalid JSON file.");

			QByteArray data = resource.readAll();
			QJsonDocument document(QJsonDocument::fromJson(data));
			scene.read(document.object());
		}
		Core core(scene, renderer);
		auto command = core.run_level();
		exit_program = command == CMD_EXIT_PROGRAM;
		scene.clear();

		if (command == CMD_NEXT_LEVEL)
			current_level++;

		if (current_level > nb_levels)
			current_level = 1;

		Settings::write_level_number(current_level);
	}
}

/**
 * Parse command line and update settings accordingly
 *
 * @param app Qt core application
 */
void parse_command_line(QCoreApplication &app)
{
	// Create command line parser and define options
	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();

	parser.addOption({ "fullscreen", "Enable fullscreen mode."});
	parser.addOption({ "window", "Enable windowed mode."});
	parser.addOption({ "file", "Level file.", "file"});
	parser.addOption({ "level", "Level number.", "number"});
	parser.addOption({ "output", "Output file.", "output"});

	parser.process(app);

	// Get current values from settings
	bool fullscreen_mode = Settings::read_fullscreen();
	auto level_file = Settings::read_level_file();
	auto level_number = Settings::read_level_number();
	auto output_file = Settings::read_output_file();

	// Update values according to actual command line
	if (parser.isSet("fullscreen"))
		fullscreen_mode = true;

	if (parser.isSet("window"))
		fullscreen_mode = false;

	if (parser.isSet("file")) {
		level_file = parser.value("file").toStdString();
		Settings::write_level_file(level_file);
	}

	if (parser.isSet("level")) {
		level_number = parser.value("level").toInt();
		level_number = std::max(level_number, 1);
		Settings::write_level_number(level_number);
	}

	if (parser.isSet("output")) {
		output_file = parser.value("output").toStdString();
		Settings::write_output_file(output_file);
	}

	Settings::write_fullscreen(fullscreen_mode);

	qDebug() << "Fullscreen   :" << fullscreen_mode;
	qDebug() << "Level file   :" << QString(level_file.c_str());
	qDebug() << "Level number :" << QString::number(level_number);
	qDebug() << "Output file  :" << QString(output_file.c_str());
}
