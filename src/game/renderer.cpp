/*
 * Zaqaru
 * Copyright © 2018, 2019, 2020 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "renderer.hpp"

#include "defs.hpp"
#include "scene.hpp"
#include "settings.hpp"

#include <GL/glu.h>
#include <QDebug>
#include <QtGlobal>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <cassert>
#include <sstream>
#include <stdexcept>

struct Color {
	float red = 0.0;
	float green = 0.0;
	float blue = 0.0;
};

struct WireFrame {
	std::vector<GLfloat> vertices;
	std::vector<GLubyte> indices;
};

struct TexturedForm {
	std::multimap<int, std::vector<GLfloat>> quads;
};

static void draw_object(const WireFrame &object, Vec3 position, Color color);


const auto DEFAULT_WIDTH = 800;
const auto DEFAULT_HEIGHT = 600;
const auto MINIMUM_WIDTH = 200;
const auto MINIMUM_HEIGHT = 150;
const auto ORTHO_NEAR = -1;
const auto ORTHO_FAR = 1;
const auto PERSPECTIVE_NEAR = 2.0;
const auto PERSPECTIVE_FAR = 40.0;
const auto TEXTURE_WIDTH = 32;

const Color BLACK       = { 0.0, 0.0, 0.0 };
const Color CYAN        = { 0.0, 1.0, 1.0 };
const Color DARK_PURPLE = { 0.5, 0.1, 0.5 };
const Color DARK_GRAY   = { 0.3, 0.3, 0.3 };
const Color GRAY        = { 0.5, 0.5, 0.5 };
const Color PURPLE      = { 1.0, 0.1, 1.0 };
const Color WHITE       = { 1.0, 1.0, 1.0 };

const auto PLAYER_COLOR = WHITE;
const auto PLAYER_MARK  = GRAY;
const auto GUARD_COLOR  = PURPLE;
const auto GUARD_MARK   = DARK_PURPLE;

namespace forms {

const WireFrame VFIELD = {
	{
		-0.4, -0.5, -0.4, -0.4,  0.5, -0.4,
		 0.4, -0.5, -0.4,  0.4,  0.5, -0.4,
		-0.4, -0.5,  0.4, -0.4,  0.5,  0.4,
		 0.4, -0.5,  0.4,  0.4,  0.5,  0.4,
	},
	{
		0, 1, 2, 3, 4, 5, 6, 7,
	}
};

const WireFrame HFIELD = {
	{
		-0.4, -0.5, -0.4,  0.4, -0.5, -0.4,
		 0.4, -0.5,  0.4, -0.4, -0.5,  0.4,
	},
	{
		0, 1, 1, 2, 2, 3, 3, 0,
	}
};

const WireFrame POD = {
	{
		-0.5, -0.5, -0.5, -0.5, -0.5,  0.5,
		-0.5,  0.5, -0.5, -0.5,  0.5,  0.5,
		 0.5, -0.5, -0.5,  0.5, -0.5,  0.5,
		 0.5,  0.5, -0.5,  0.5,  0.5,  0.5,
		 0.0,  0.4,  0.0,
	},
	{
		0, 4, 1, 5, 2, 6, 3, 7,
		0, 1, 4, 5, 2, 3, 6, 7,
		0, 2, 1, 3, 4, 6, 5, 7,
		2, 8, 3, 8, 7, 8, 6, 8,
	}
};

const WireFrame SLOPE = {
	{
		-0.5, -0.5, -0.5,
		-0.5, -0.5,  0.5,
		-0.5,  0.5, -0.5,
		 0.5, -0.5, -0.5,
		 0.5, -0.5,  0.5,
		 0.5,  0.5, -0.5,
	},
	{
		0, 3, 1, 4, 2, 5, 0, 1, 3, 4, 2, 1, 5, 4, 0, 2, 3, 5,
	}
};

const WireFrame COIN = {
	{
		-0.2, -0.2, -0.2, -0.2, -0.2,  0.2,
		 0.2, -0.2, -0.2,  0.2, -0.2,  0.2,
		 0.0,  0.2,  0.0,
	},
	{
		0, 2, 1, 3, 0, 1, 2, 3, 0, 4, 1, 4, 2, 4, 3, 4,
	}
};

const WireFrame START = {
	{
		0.0, -0.5, 0.0,	0.0,  0.0, 0.0,
	},
	{
		0, 1,
	}
};

const WireFrame RUNNER = {
	{
		-0.3, 0.0, -0.2, -0.3, 0.1, -0.2,
		-0.1, 0.1, -0.2,  0.0, 0.4,  0.0,
		 0.2, 0.3,  0.2,  0.2, 0.0,  0.2,
		 0.3, 0.0,  0.2,  0.1, 0.7,  0.0,
		 0.2, 0.5, -0.2,  0.4, 0.5, -0.2,
		-0.1, 0.7,  0.2, -0.2, 0.5,  0.2,
		 0.1, 0.8,  0.0,
	},
	{
		 0,  1,  1,  2,  2,  3,  3,  4,  4,  5,  5,  6,
		 3,  7,  7,  8,  8,  9,  7, 10, 10, 11,  7, 12,
	}
};

const WireFrame MARK = {
	{
		 0.4, -0.4,  0.4,  0.4, -0.4, -0.4,
		-0.4, -0.4, -0.4, -0.4, -0.4,  0.4,
	},
	{
		0, 1, 1, 2, 2, 3, 3, 0,
	}
};

const std::vector<GLfloat> CUBE_SIDES = {
	// Vertices ----  Normals --------   Tex Coords
	-0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  0.0, 0.0, // Front
	 0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  1.0, 0.0,
	 0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  1.0, 1.0,
	-0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  0.0, 1.0,
	 0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  0.0, 0.0, // Back
	-0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  1.0, 0.0,
	-0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  1.0, 1.0,
	 0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  0.0, 1.0,
	 0.5, -0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 0.0, // Left
	 0.5, -0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 0.0,
	 0.5,  0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 1.0,
	 0.5,  0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 1.0,
	-0.5, -0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 0.0, // Right
	-0.5, -0.5,  0.5, -1.0,  0.0,  0.0,  1.0, 0.0,
	-0.5,  0.5,  0.5, -1.0,  0.0,  0.0,  1.0, 1.0,
	-0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 1.0,
};

const std::vector<GLfloat> CUBE_TOP = {
	// Vertices ----  Normals -----  Tex Coords
	-0.5,  0.5,  0.5, 0.0, 1.0, 0.0, 0.0, 0.0,
	 0.5,  0.5,  0.5, 0.0, 1.0, 0.0, 1.0, 0.0,
	 0.5,  0.5, -0.5, 0.0, 1.0, 0.0, 1.0, 1.0,
	-0.5,  0.5, -0.5, 0.0, 1.0, 0.0, 0.0, 1.0,
};

const std::vector<GLfloat> CUBE_BOTTOM = {
	// Vertices ----  Normals ------  Tex Coords
	-0.5, -0.5, -0.5, 0.0, -1.0, 0.0, 0.0, 0.0,
	 0.5, -0.5, -0.5, 0.0, -1.0, 0.0, 1.0, 0.0,
	 0.5, -0.5,  0.5, 0.0, -1.0, 0.0, 1.0, 1.0,
	-0.5, -0.5,  0.5, 0.0, -1.0, 0.0, 0.0, 1.0,
};

const std::vector<GLfloat> CUBE_BACK = {
	// Vertices ----  Normals --------   Tex Coords
	 0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  0.0, 0.0,
	-0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  1.0, 0.0,
	-0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  1.0, 1.0,
	 0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  0.0, 1.0,
};

const std::vector<GLfloat> CUBE_FRONT = {
	// Vertices ----  Normals --------   Tex Coords
	-0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  0.0, 0.0,
	 0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  1.0, 0.0,
	 0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  1.0, 1.0,
	-0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  0.0, 1.0,
};

const std::vector<GLfloat> SLOPE_SIDES = {
	// Vertices ----  Normals --------   Tex Coords
	 0.5, -0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 0.0, // Left
	 0.5, -0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 0.0,
	 0.5,  0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 1.0,
	 0.5, -0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 1.0, // same as first
	-0.5, -0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 0.0, // Right
	-0.5, -0.5,  0.5, -1.0,  0.0,  0.0,  1.0, 0.0,
	-0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  1.0, 1.0,
	-0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 1.0,
};

const std::vector<GLfloat> USLOPE_SIDES = {
	// Vertices ----  Normals --------   Tex Coords
	 0.5, -0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 0.0, // Left
	 0.5,  0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 0.0, // same as next
	 0.5,  0.5, -0.5,  1.0,  0.0,  0.0,  1.0, 1.0,
	 0.5,  0.5,  0.5,  1.0,  0.0,  0.0,  0.0, 1.0,
	-0.5, -0.5,  0.5, -1.0,  0.0,  0.0,  1.0, 0.0, // Right
	-0.5,  0.5,  0.5, -1.0,  0.0,  0.0,  1.0, 1.0,
	-0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 1.0,
	-0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  0.0, 0.0,
};

const std::vector<GLfloat> SLOPE_TOP = {
	// Vertices ----  Normals -----  Tex Coords
	-0.5, -0.5,  0.5, 0.0, 1.0, 1.0, 0.0, 0.0,
	 0.5, -0.5,  0.5, 0.0, 1.0, 1.0, 1.0, 0.0,
	 0.5,  0.5, -0.5, 0.0, 1.0, 1.0, 1.0, 1.0,
	-0.5,  0.5, -0.5, 0.0, 1.0, 1.0, 0.0, 1.0,
};

const std::vector<GLfloat> SLOPE_BOTTOM = {
	// Vertices ----  Normals -------  Tex Coords
	-0.5,  0.5, -0.5, 0.0, -1.0, -1.0, 0.0, 0.0,
	 0.5,  0.5, -0.5, 0.0, -1.0, -1.0, 1.0, 0.0,
	 0.5, -0.5,  0.5, 0.0, -1.0, -1.0, 1.0, 1.0,
	-0.5, -0.5,  0.5, 0.0, -1.0, -1.0, 0.0, 1.0,
};

const TexturedForm TBLOCK = {
	{
		{ 0, CUBE_SIDES },
		{ 1, CUBE_TOP },
		{ 2, CUBE_BOTTOM },
	}
};

const TexturedForm TTOUGH = {
	{
		{ 3, CUBE_SIDES },
		{ 1, CUBE_TOP },
		{ 2, CUBE_BOTTOM },
	}
};

const TexturedForm TSLOPE = {
	{
		{ 3, CUBE_BACK },
		{ 3, SLOPE_SIDES },
		{ 3, SLOPE_TOP },
		{ 2, CUBE_BOTTOM },
	}
};

const TexturedForm TUSLOPE = {
	{
		{ 3, CUBE_FRONT },
		{ 3, USLOPE_SIDES },
		{ 3, SLOPE_BOTTOM },
		{ 1, CUBE_TOP },
	}
};

} // namespace forms

/**
 * Constructor.
 */
Renderer::Renderer(const Scene &s)
	: scene(s)
{
}

/**
 * Destructor. Close window.
 */
Renderer::~Renderer()
{
	close();
}

/**
 * Initialize SDL window with OpenGL context
 */
void Renderer::initialize()
{
	int width = DEFAULT_WIDTH;
	int height = DEFAULT_HEIGHT;
	int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

	if (Settings::read_fullscreen()) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	sdl_window = SDL_CreateWindow(
		PROJECT_NAME " " PROJECT_VERSION VERSION_STATUS,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		flags);

	if (sdl_window == nullptr)
		throw_sdl_exception("SDL window creation failed.");

	auto sdl_renderer = SDL_CreateRenderer(sdl_window, -1, 0);
	if (sdl_renderer == nullptr)
		throw_sdl_exception("SDL renderer creation failed.");

	auto gl_context = SDL_GL_CreateContext(sdl_window);
	if (gl_context == nullptr)
		throw_sdl_exception("SDL OpenGL context creation failed.");

	change_size(width, height);

	create_default_textures(TEXTURE_WIDTH);
	textures.resize(bitmaps.size());
	glGenTextures(bitmaps.size(), textures.data());

	for (unsigned int i = 0; i < textures.size(); i++)
		setup_texture(i, TEXTURE_WIDTH);
}

/**
 * Throws a runtime error exception with customized message and SDL message
 *
 * @param message Error to include in thrown exception
 *
 * The exception message will also contain the SDL_GetError message
 */
void Renderer::throw_sdl_exception(const char *message)
{
	std::stringstream s;

	if (message != nullptr)
		s << message << std::endl;

	s << SDL_GetError();
	throw std::runtime_error(s.str());
}

/**
 * Register hardcoded textures
 */
void Renderer::setup_texture(unsigned int index, unsigned int width)
{
	glBindTexture(GL_TEXTURE_2D, textures[index]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, width, 0, GL_RGB,
		     GL_FLOAT, &bitmaps[index][0]);
}

/**
 * Close SDL window
 */
void Renderer::close()
{
	if (sdl_window != nullptr) {
		SDL_DestroyWindow(sdl_window);
		sdl_window = nullptr;
	}
}

/**
 * Swap window
 */
void Renderer::swap_window()
{
	SDL_GL_SwapWindow(sdl_window);
}

/**
 * Reset viewport dimensions/location after window size changes
 */
void Renderer::change_size(int x, int y)
{
	assert(x >= 0);
	assert(y >= 0);

	if (x < MINIMUM_WIDTH || y < MINIMUM_HEIGHT) {
		viewport_ok = false;
		return;
	}

	viewport_ok = true;
	viewport_width = x;
	viewport_height = y;

	double ratio_xy = (double) DEFAULT_WIDTH / DEFAULT_HEIGHT;

	if ((double) x / y > ratio_xy) {
		viewport_offset_x = (x - y * ratio_xy) / 2;
		viewport_offset_y = 0;
	} else {
		viewport_offset_y = (y - x / ratio_xy) / 2;
		viewport_offset_x = 0;
	}
}

/**
 * Toggle between window mode and fullscreen mode
 */
void Renderer::toggle_fullscreen()
{
	Q_ASSERT(sdl_window);

	bool fullscreen_mode = Settings::read_fullscreen();

	int flags = fullscreen_mode ? 0 : SDL_WINDOW_FULLSCREEN_DESKTOP;
	int status = SDL_SetWindowFullscreen(sdl_window, flags);

	if (status == 0)
		Settings::write_fullscreen(!fullscreen_mode);
	else
		qWarning("SDL Fullscreen toggle failed [%s]", SDL_GetError());
}

/**
 * Render game view
 */
void Renderer::render()
{
	assert(sdl_window);

	if (!viewport_ok) {
		glClearColor(0.2, 0.0, 0.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		return;
	}

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, viewport_width, viewport_height);

	render_scene();
	render_overlay();
}

/**
 * Render the main 3D scene
 */
void Renderer::render_scene() const
{
	GLfloat specular[] = { 1.0, 9.0, 7.0, 1.0 };
	GLfloat diffuse[] = { 0.6, 0.9, 0.9, 1.0 };
	GLfloat shininess[] = { 100.0 };

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	auto ratio = (GLfloat) viewport_width / (GLfloat) viewport_height;
	gluPerspective(60.0f, ratio, PERSPECTIVE_NEAR,PERSPECTIVE_FAR);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Vec3 p = scene.camera_position();
	Vec3 t = scene.camera_target();
	gluLookAt(p.x, p.y, p.z, t.x, t.y, t.z, 0.0, 1.0, 0.0);

	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);

	auto lights = scene.get_lights();

	for (unsigned i = 0; i < 8 && i < lights.size(); i++) {
		Vec3 light = lights[i];
		GLfloat array[4];
		array[0] = light.x;
		array[1] = light.y;
		array[2] = light.z;
		array[3] = 1.0;
		int id = GL_LIGHT0 + i;
		glLightfv(id, GL_POSITION, array);
		glLightfv(id, GL_DIFFUSE, diffuse);
		glLightfv(id, GL_SPECULAR, specular);
		glEnable(id);
	}

	glEnable(GL_FOG) ;
	glHint (GL_FOG_HINT, GL_NICEST);
	GLfloat fogcolor[4] = {0.1, 0.1, 0.1, 1} ;
	glFogi (GL_FOG_MODE, GL_LINEAR) ;
	glFogfv(GL_FOG_COLOR, fogcolor) ;
	glFogf(GL_FOG_START, 20.0) ;
	glFogf(GL_FOG_END, 35.0) ;

	render_textured_elements();

	glDisable(GL_LIGHTING);

	render_wireframe_elements();
	render_objects();
	render_ai_graph();
}

/**
 * Render wireframe elements
 */
void Renderer::render_wireframe_elements() const
{
	auto elements = scene.get_elements();

	glDisable(GL_LIGHTING);
	glEnableClientState(GL_VERTEX_ARRAY);

	for (Element *element : elements) {
		if (!element->is_active() || element->is_destroyed())
			continue;

		Color color = { 1.0, 1.0, 1.0 };
		auto position = element->grid();
		switch (element->type()) {
		case Element::USTREAM:
			color = { 0.6, 0.5, 0.1 };
			draw_object(forms::VFIELD, Vec3(position), color);
			break;
		case Element::HSTREAM:
			color = { 0.6, 0.5, 0.1 };
			draw_object(forms::HFIELD, Vec3(position), color);
			break;
		case Element::VFIELD:
			color = { 0.8, 0.8, 0.8 };
			draw_object(forms::VFIELD, Vec3(position), color);
			break;
		case Element::HFIELD:
			color = { 0.7, 0.3, 0.3 };
			draw_object(forms::HFIELD, Vec3(position), color);
			break;
		case Element::EXIT:
			color = { 0.5, 1.0, 0.5 };
			break;
		case Element::JUMPER:
		case Element::TELEPORT:
			draw_object(forms::POD, Vec3(position), color);
			break;
		default:
			break;
		}
	}
	glDisableClientState(GL_VERTEX_ARRAY);
}

/**
 * Draw a form described in WireFrame object
 */
static void draw_object(const WireFrame &object, Vec3 position, Color color)
{
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glColor4f(color.red, color.green, color.blue, 1.0);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glVertexPointer(3, GL_FLOAT, 0, object.vertices.data());
	glDrawElements(GL_LINES, object.indices.size(),
		       GL_UNSIGNED_BYTE, object.indices.data());
	glPopMatrix();
}

/**
 * Render 2D overlay
 */
void Renderer::render_overlay() const
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, viewport_width, 0, viewport_height, ORTHO_NEAR, ORTHO_FAR);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (display_frame) {
		auto x1 = viewport_offset_x;
		auto y1 = viewport_offset_y;
		auto x2 = viewport_width - viewport_offset_x;
		auto y2 = viewport_height - viewport_offset_y;

		glColor3f(0.1, 0.1, 0.1);
		glBegin(GL_LINE_LOOP);
		glVertex3f(x1, y1, 0);
		glVertex3f(x2, y1, 0);
		glVertex3f(x2, y2, 0);
		glVertex3f(x1, y2, 0);
		glEnd();
	}

	if (display_dashboard) {
		glBegin(GL_QUADS);
		glVertex3f(10, 10, 0);
		glVertex3f(viewport_width - 10, 10, 0);
		glVertex3f(viewport_width - 10, 50, 0);
		glVertex3f(10, 50, 0);
		glEnd();
	}
}

/**
 * Render objects and players
 */
void Renderer::render_objects() const
{
	auto elements = scene.get_elements();

	glDisable(GL_LIGHTING);
	glEnableClientState(GL_VERTEX_ARRAY);

	for (Element *element : elements) {
		for (auto object : element->get_objects()) {
			auto position = element->grid();
			position.y += object.y;
			Color color = { 1.0, 1.0, 1.0 };
			switch (object.type) {
			case Object::COIN:
				color = { 1.0, 1.0, 0.1 };
				draw_object(forms::COIN, Vec3(position), color);
				break;
			case Object::GUARD:
				color = { 1.0, 0.0, 0.0 };
				draw_object(forms::START, Vec3(position), color);
				break;
			case Object::PLAYER:
				color = { 0.0, 1.0, 0.0 };
				draw_object(forms::START, Vec3(position), color);
				break;
			case Object::RESPAWN:
				color = { 0.3, 0.4, 0.2 };
				draw_object(forms::START, Vec3(position), color);
				break;
			default:
				qWarning() << "Non-rendered object";
			}
		}
	}

	const Unit &player = scene.get_player();
	draw_object(forms::RUNNER, player.coordinates(), PLAYER_COLOR);
	draw_object(forms::MARK, player.get_base()->coordinates(), PLAYER_MARK);

	auto guards = scene.get_guards();
	for (Guard *guard : guards) {
		draw_object(forms::RUNNER, guard->coordinates(), GUARD_COLOR);
		draw_object(forms::MARK, guard->get_base()->coordinates(), GUARD_MARK);

	}
	glDisableClientState(GL_VERTEX_ARRAY);
}

/**
 * Render AI graph and paths
 */
void Renderer::render_ai_graph() const
{
	glDisable(GL_LIGHTING);

	auto guards = scene.get_guards();

	for (Guard *guard : guards) {
		auto dots = guard->get_path();
		if (dots.size() > 1) {
			glColor3f(0.1, 0.9, 0.1);
			glBegin(GL_LINE_STRIP);
			for (Vec3 p : dots)
				glVertex3f(p.x, p.y, p.z);
			glEnd();
		}
	}
	if (!guards.empty()) {
		auto dots = guards.front()->get_graph();
		glColor3f(0.1, 0.1, 0.5);
		glBegin(GL_LINES);
		for (Vec3 p : dots)
			glVertex3f(p.x, p.y, p.z);
		glEnd();
	}
}

/**
 * Create default textures
 */
void Renderer::create_default_textures(int width)
{
	// 0: Side of common blocks
	std::vector<GLfloat> bitmap;
	for (int y = 0; y < width; ++y) {
		for (int x = 0; x < width; ++x) {
			Color color = DARK_GRAY;
			int w = width - 1;
			if (y == w || y == width / 2 - 1)
				color = GRAY;

			if (y < width / 2 && x == width / 3)
				color = GRAY;

			if (y >= width / 2 && x == 2 * width / 3)
				color = GRAY;

			bitmap.push_back(color.red);
			bitmap.push_back(color.green);
			bitmap.push_back(color.blue);
		}
	}
	bitmaps.push_back(bitmap);

	// 1: Top of blocks
	bitmap.clear();
	for (int y = 0; y < width; ++y) {
		for (int x = 0; x < width; ++x) {
			Color color = DARK_GRAY;
			int w = width - 1;
			if (x == 0 || y == 0 || x == w || y == w)
				color = GRAY;

			bitmap.push_back(color.red);
			bitmap.push_back(color.green);
			bitmap.push_back(color.blue);
		}
	}
	bitmaps.push_back(bitmap);

	// 2: Bottom of blocks
	bitmap.clear();
	for (int y = 0; y < width; ++y) {
		for (int x = 0; x < width; ++x) {
			Color color = DARK_GRAY;
			bitmap.push_back(color.red);
			bitmap.push_back(color.green);
			bitmap.push_back(color.blue);
		}
	}
	bitmaps.push_back(bitmap);

	// 3: Indestructible blocks, slopes
	bitmap.clear();
	for (int y = 0; y < width; ++y) {
		for (int x = 0; x < width; ++x) {
			Color color = DARK_GRAY;
			int w = width - 1;
			if (y == w || y == width / 2 - 1)
				color = GRAY;

			bitmap.push_back(color.red);
			bitmap.push_back(color.green);
			bitmap.push_back(color.blue);
		}
	}
	bitmaps.push_back(bitmap);
}

/**
 * Render elements with textured form
 */
void Renderer::render_textured_elements() const
{
	auto elements = scene.get_elements();
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glColor4f(1.0, 1.0, 1.0, 1.0);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	for (Element *element : elements) {
		if (!element->is_active() || element->is_destroyed())
			continue;

		auto pos = element->grid();
		auto angle = element->get_angle();
		switch (element->type()) {
		case Element::BLOCK:
		case Element::TRAP:
			draw_textured_object(forms::TBLOCK, Vec3(pos), angle);
			break;
		case Element::TOUGH:
			draw_textured_object(forms::TTOUGH, Vec3(pos), angle);
			break;
		case Element::SLOPE:
			draw_textured_object(forms::TSLOPE, Vec3(pos), angle);
			break;
		case Element::USLOPE:
			draw_textured_object(forms::TUSLOPE, Vec3(pos), angle);
			break;
		default:
			break;
		}
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glDisable(GL_TEXTURE_2D);
}

/**
 * Draw a form described in TexturedForm object
 */
void Renderer::draw_textured_object(const TexturedForm &object,
				    const Vec3 &position, int angle) const
{
	for (auto it : object.quads) {
		int id = it.first;
		auto &data = it.second;

		glBindTexture(GL_TEXTURE_2D, textures[id]);

		glPushMatrix();

		glTranslatef(position.x, position.y, position.z);
		glRotatef(angle, 0.0, 1.0, 0.0);
		int vertex_element_size = 8;
		auto stride = vertex_element_size * sizeof(GLfloat);

		glVertexPointer(3, GL_FLOAT, stride, &it.second[0]);
		glNormalPointer(GL_FLOAT, stride, &it.second[3]);
		glTexCoordPointer(2, GL_FLOAT, stride, &it.second[6]);

		glDrawArrays(GL_QUADS, 0, data.size() / vertex_element_size);

		glPopMatrix();
	}
}
