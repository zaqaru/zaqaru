/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#include "box.hpp"
#include "defs.hpp"
#include "vec.hpp"

#include <QJsonObject>

#include <set>
#include <vector>

class Scene;
class Unit;

/**
 * Object
 */
struct Object {
	enum Type {
		NONE, COIN, PLAYER, GUARD, RESPAWN
	};

	Type type;
	double y;

	bool is_game_object() const {
		return type != PLAYER && type != GUARD && type != RESPAWN;
	}
};

/**
 * Element
 */
class Element
{
public:
	enum Type {
		NONE,
		BLOCK   = 1,
		TOUGH   = 2,
		VFIELD  = 3,
		HFIELD  = 4,
		TRAP    = 5,
		USTREAM = 6,
		HSTREAM = 7,
		SLOPE   = 8,
		USLOPE  = 9,
		EXIT,
		JUMPER,
		TELEPORT,
	};

	enum Property {
		APPEAR_ON_UNLOCK    = 1,
		DISAPPEAR_ON_UNLOCK = 2,
		CLIMBABLE,
		CROSSABLE,
		CROSSABLE_BOTTOM,
		CROSSABLE_TOP,
		DESTROYABLE,
		OBSTRUCTING,
		OBSTRUCTING_BOTTOM,
		OBSTRUCTING_TOP,
		SLIDING,
		WALKABLE_BOTTOM,
		WALKABLE_TOP,
	};

	Element(Scene *root);
	Element(Scene *root, Type t, const Vec3i &coordinates);
	virtual ~Element();

	Vec3 coordinates() const;

	void add_object(Object::Type type, double y);
	void block(const Unit *);
	void clear();
	bool destroy();

	std::vector<Object> get_objects() const;
	int get_camera_id() const;
	Object give_object_at(const Box &source);
	void remove_objects(double y1, double y2);
	void set_camera_id(int);
	void set_angle(unsigned int a);
	void set_property(Property);
	void on_move_inside();
	void unblock();
	void unlock();
	void update(gametime dt);

	double bottom() const;
	Box get_aabb() const;
	double get_aabb_top() const;
	int get_angle() const;
	Vec3 get_exit_point() const;
	Vec3 get_field_velocity() const;
	double get_top(double x, double z) const;

	Vec3i grid() const;
	int grid_x() const;
	int grid_y() const;
	int grid_z() const;
	Type type() const;

	bool has_field() const;
	bool is_active() const;
	bool is_blocked(const Unit *unit) const;
	bool is_climbable() const;
	bool is_crossable(Axis xz, Direction dir) const;
	bool is_crossable_bottom() const;
	bool is_crossable_inside() const;
	bool is_crossable_top() const;
	bool is_destroyable() const;
	bool is_destroyed() const;
	bool is_inside(const Vec3 &coordinates) const;
	bool is_obstructing(bool dynamic = true) const;
	bool is_obstructing_bottom(bool dynamic = true) const;
	bool is_obstructing_top(bool dynamic = true) const;
	bool is_slider() const;
	bool is_walkable_bottom() const;
	bool is_walkable_bottom(const Unit *unit) const;
	bool is_walkable_top() const;
	bool is_walkable_top(const Unit *unit) const;
	bool is_walkable_top(Axis xz, Direction dir) const;

	// Serialization
	void read(const QJsonObject &json);
	void write(QJsonObject &element_json) const;

private:
	enum State {
		NORMAL, SHRINK, DESTROYED, EXPAND, REVEALED,
	};

	enum Field {
		NO_FIELD, VERTICAL_FIELD, HORIZONTAL_FIELD,
	};

	bool has_property(Property property) const;

	void set_state(State, gametime);
	void setup();

	std::vector<Object> objects;
	std::set<Property> properties;
	Vec3i position;
	int angle = 0;
	Type element_type = NONE;
	State state = NORMAL;
	Field field = NO_FIELD;

	Scene *scene = nullptr;

	bool active = true;
	int camera_id = 0;
	gametime time = 0;
	const Unit *blocker = nullptr;
};

#endif // ELEMENT_HPP
