/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HELPER_HPP
#define HELPER_HPP

#include "defs.hpp"
#include "vec.hpp"

namespace helper {

int compare(double d1, double d2);
double distance(const Vec3 &a, const Vec3 &b);
bool equality(double d1, double d2);
bool equality(const Vec3 &u, const Vec3 &v);
Direction get_direction(double x);
bool in_range(double x, double x1, double x2);

void move_closer(double &value, double target, double step);
void move_closer_radians(double &value, double target, double step);
void move_closer(Vec3 &v, const Vec3 &target, double delta);

bool string_ends_with(const std::string &, const std::string &);
}

#endif // HELPER_HPP
