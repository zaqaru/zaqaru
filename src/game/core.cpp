/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core.hpp"

#include "defs.hpp"
#include "helper.hpp"
#include "renderer.hpp"
#include "settings.hpp"

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <SDL2/SDL.h>

static UserCommand read_keyboard_event(const SDL_Event &);
static void handle_window_event(const SDL_Event &, Renderer &);

/**
 * Constructor
 */
Core::Core(Scene &s, Renderer &r)
	: renderer(r), scene(s)
{
}

/**
 * Destructor
 */
Core::~Core()
{
}

/**
 * Run the current level
 */
UserCommand Core::run_level()
{
	int frame_delay = 1;

	auto last_frame = SDL_GetTicks();

	while (true) {

		SDL_Delay(frame_delay);

		auto current_frame = SDL_GetTicks();
		auto dt = current_frame - last_frame;

		if (dt < 15) {
			SDL_Delay(1);
			continue;
		}
		if (dt > 40)
			dt = 40;

		SDL_Event event;
		UserCommand command = CMD_NONE;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				command = CMD_EXIT_PROGRAM;
				break;
			case SDL_WINDOWEVENT:
				handle_window_event(event, renderer);
				break;
			case SDL_KEYDOWN:
				command = read_keyboard_event(event);
				break;
			default: break;
			}
		}

		switch (command) {
		case CMD_EXIT_PROGRAM:
		case CMD_RESTART_LEVEL:
			return command;
		case CMD_FULLSCREEN:
			renderer.toggle_fullscreen();
			break;
		case CMD_SAVE:
			save();
			break;
		default:
			break;
		}

		Scene::Status status = scene.update(dt, command);
		renderer.render();
		renderer.swap_window();

		switch (status) {
		case Scene::END_WIN:
			return CMD_NEXT_LEVEL;
		case Scene::END_FAIL:
			return CMD_RESTART_LEVEL;
		default: break;
		}

		last_frame = current_frame;
	}

	return CMD_NONE;
}

/*
 * Read keyboard input and convert it into a user command
 */
UserCommand read_keyboard_event(const SDL_Event &event)
{
	UserCommand command = CMD_NONE;

	if (event.key.repeat == 1)
		return command;

	switch (event.key.keysym.sym) {
	case SDLK_ESCAPE:
		command = CMD_RESTART_LEVEL;
		break;
	case SDLK_q:
		command = CMD_EXIT_PROGRAM;
		break;
	case SDLK_f:
		command = CMD_FULLSCREEN;
		break;
	case SDLK_KP_4:
	case SDLK_LEFT:
		command = CMD_LEFT;
		break;
	case SDLK_KP_6:
	case SDLK_RIGHT:
		command = CMD_RIGHT;
		break;
	case SDLK_KP_8:
	case SDLK_UP:
		command = CMD_UP;
		break;
	case SDLK_KP_2:
	case SDLK_DOWN:
		command = CMD_DOWN;
		break;
	case SDLK_KP_5:
	case SDLK_SPACE:
		command = CMD_STOP;
		break;
	case SDLK_KP_7:
	case SDLK_LCTRL:
	case SDLK_w:
	case SDLK_z:
		command = CMD_ACTION_LEFT;
		break;
	case SDLK_KP_9:
	case SDLK_LALT:
	case SDLK_x:
		command = CMD_ACTION_RIGHT;
		break;
	case SDLK_s:
		command = CMD_SAVE;
		break;
	default:
		command = CMD_NONE;
		break;
	}

	return command;
}

/**
 * Handle window-related event (resize)
 */
void handle_window_event(const SDL_Event &event, Renderer &renderer)
{
	Q_ASSERT(event.type == SDL_WINDOWEVENT);

	switch (event.window.event) {
	case SDL_WINDOWEVENT_RESIZED:
	case SDL_WINDOWEVENT_SIZE_CHANGED:
		renderer.change_size(event.window.data1, event.window.data2);
		break;
	default: break;
	}
}

/**
 * Export current scene to file
 *
 * The export file name is the output file defined in the settings
 */
void Core::save() const
{
	auto filename = Settings::read_output_file();
	if (filename.empty()) {
		qWarning("Invalid output file.");
		return;
	}

	QFile file(filename.c_str());

	if (!file.open(QIODevice::WriteOnly)) {
	    qWarning("Could not open file.");
	    return;
	}

	QJsonObject json;
	scene.write(json);
	QJsonDocument document(json);

	if (helper::string_ends_with(filename, ".json"))
		file.write(document.toJson());
	else
		file.write(document.toBinaryData());

	qDebug() << "Scene saved to file" << filename.c_str();
}
