/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit.hpp"

#include "helper.hpp"
#include "scene.hpp"

#include <QDebug>
#include <QtGlobal>

#include <cmath>

using helper::compare;
using helper::equality;
using helper::get_direction;
using helper::move_closer;

const gametime BLOCKED_GUARD_DURATION = 2000;
const double EXIT_SPEED = 0.004;
const double SLIDE_HSPEED = 0.004;

/**
 * Constructor
 * @param element Base element
 * @param height Height above base element
 */
Unit::Unit(Scene *const root, const Vec3 &absolute_position)
	: position(absolute_position),
	  intent(NONE),
	  state(NORMAL),
	  scene(root)
{
	Q_ASSERT(scene);

	base = scene->find_element_below(Vec3i(absolute_position));
	if (base != nullptr) {
		position = absolute_position - base->coordinates();
	}
}

/**
 * Destructor
 */
Unit::~Unit()
{
}

/**
 * Return absolute position
 */
Vec3 Unit::coordinates() const
{
	if (base)
		return base->coordinates() + position;
	else
		return position;
}

/**
 * Update unit
 */
void Unit::update(gametime dt, const std::vector<Unit *> &obstacles)
{
	if (base == nullptr) {
		qWarning() << "Update unit with no base element.";
		return;
	}

	trap_time = dt < trap_time ? trap_time - dt : 0;

	if (state == TRAP_DOWN && trap_time == 0) {
		state = TRAP_OUT;
		set_intent();
	}

	if (state == TRAP_OUT) {
		exit_trap(dt);
		return;
	}

	bool done = check_fall(dt);

	if (!done)
		done = check_sliding(dt);

	if (!done)
		done = check_field(dt);

	if (!done)
		done = check_action();

	if (!done)
		done = check_vertical_move(dt, obstacles);

	if (!done)
		done = check_horizontal_move(dt, obstacles);

	check_object_presence();
}

/**
 * Center player in/on its base element, on X axis
 * @param dt Time step
 */
void Unit::autocenter_x(gametime dt)
{
	auto delta = climb_speed * dt;
	helper::move_closer(position.x, 0.0, delta);
}

/**
 * Center player in/on its current element, on Y axis
 * @param dt Time step
 */
void Unit::autocenter_y(gametime dt)
{
	auto delta = climb_speed * dt;
	auto target = compare(position.y, 0.0) > 0
			? base->get_aabb_top()
			: base->bottom();
	helper::move_closer(position.y, target, delta);
}

/**
 * Center player in/on its current element, on Z axis
 * @param dt Time step
 */
void Unit::autocenter_z(gametime dt)
{
	auto delta = climb_speed * dt;
	helper::move_closer(position.z, 0.0, delta);
}

/**
 * Let the unit fall if the conditions match
 * @param dt Time step
 * @return true if time was consumed
 */
bool Unit::check_fall(gametime dt)
{
	Q_ASSERT(base != nullptr);

	bool transfer = false; // change base element to the one below?
	double dy = fall_speed * dt;
	double top = base->get_top(position.x, position.z);
	double bottom = base->bottom();

	int at_top = compare(position.y - dy, top);
	int at_bottom = compare(position.y - dy, bottom);
	bool walkable_top = base->is_walkable_top(this);
	bool walkable_bottom = base->is_walkable_bottom(this);

	// Already on top ground
	if (walkable_top && equality(position.y, top))
		return false;

	// Inside element, but no reason to fall, skip.
	if (base->is_climbable() && base->is_inside(position))
		return false;

	// Already on bottom ground
	if (walkable_bottom && equality(position.y, bottom))
		return false;

	// Checking situations requiring update
	if (at_top > 0 || (!walkable_top && at_bottom > 0)) {
		position.y -= dy;
		autocenter_x(dt);
		autocenter_z(dt);
		if (at_top <= 0) {
			base->on_move_inside();
			if (state == NORMAL && trappable() &&
			    base->is_destroyed() && !base->is_blocked(this)) {
				base->block(this);
				state = TRAP_IN;
			}
		}
	} else if (at_bottom <= 0) {
		// At bottom, or go there
		dy = position.y - bottom; // actual dy to the limit
		position.y = bottom;
		if (state == TRAP_IN) {
			state = TRAP_DOWN;
			trap_time = BLOCKED_GUARD_DURATION;
		}
		transfer = !walkable_bottom && state == NORMAL;
	} else if (walkable_top && at_top <= 0) {
		// Landing (top)
		dy = position.y - top; // actual dy to the limit
		position.y = top; // go to ground
	} else {
		Q_ASSERT(!"Unexpected condition");
	}

	if (transfer)
		transfer_to_lower_element();

	return true;
}

/**
 * Transfer to the element below the current one
 */
void Unit::transfer_to_lower_element()
{
	Element *lower = scene->find_element_below(base->grid() - vec3i::Y1);

	if (lower == nullptr) {
		// TODO game over instead of this
		position.y += 1.0;
		qDebug() << "Fall out!";
		return;
	}
	change_base(lower);
}

/**
 * Move player horizontally
 * @param dt Time step
 * @return true if time is consumed
 */
bool Unit::check_horizontal_move(gametime dt,
				 const std::vector<Unit *> &obstacles)
{
	double dx = 0.0;
	double dz = 0.0;

	switch (intent) {
	case Request::GO_XN: dx = -walk_speed * dt; break;
	case Request::GO_ZP: dz = walk_speed * dt; break;
	case Request::GO_ZN: dz = -walk_speed * dt; break;
	case Request::GO_XP: dx = walk_speed * dt; break;
	default: break; // None or vertical: nothing done
	}

	// Inside an element that forbids to get out
	if (compare(position.y, base->get_aabb_top()) < 0
	    && !base->is_crossable_inside())
		return true; // consume time

	// Collision with other units
	if (check_collisions(obstacles))
		return true;

	// Other cases: check against obstacles and move
	if (!equality(dx, 0.0)) {
		if (check_stop(dx, AXIS_X, get_direction(dx))) {
			position.x += dx;
			autocenter_y(dt);
			autocenter_z(dt);
			check_side_limits(AXIS_X, get_direction(dx));
		}
		return true;
	} else if (!equality(dz, 0.0)) {
		if (check_stop(dz, AXIS_Z, get_direction(dz))) {
			position.z += dz;
			autocenter_x(dt);
			autocenter_y(dt);
			check_side_limits(AXIS_Z, get_direction(dz));
		}
		return true;
	}
	return false;
}

/**
 * Check if unit can continue its movement horizontally
 * @param delta Movement quantity
 * @param axis Axis of movement
 * @param direction Direction of movement
 * @return true if the unit can continue
 */
bool Unit::check_stop(double delta, Axis axis, Direction direction)
{
	Q_ASSERT(axis == AXIS_X || axis == AXIS_Z);
	Q_ASSERT(direction == DIR_UPW || direction == DIR_DNW);

	double &current = (axis == AXIS_X) ? position.x : position.z;
	bool upwards = (direction == DIR_UPW);
	bool beyond = false;

	if (upwards && compare(current + delta, 0.0) >= 0)
		beyond = true;

	if (!upwards && compare(current + delta, 0.0) <= 0)
		beyond = true;

	if (!beyond)
		return true;

	Vec3i v = scene->find_obstacle_candidate(base,
						 position.y,
						 axis, direction);

	// Stop if there's an obstacle
	const Element *obstacle = scene->neighbour(base, v);
	if (obstacle) {
		if (obstacle->is_obstructing()) {
			set_intent(STOP);
			current = 0.0;
			return false;
		}
	}

	// Stop if there's a cliff and nothing under
	const Element *next = scene->neighbour(base, v - vec3i::Y1);
	if (!next) {
		const Element *lower = scene->lower_neighbour(base,
							     { v.x, 0, v.z });
		if (!lower) {
			set_intent(STOP);
			current = 0.0;
			return false;
		}
	}
	return true;
}

/**
 * Handle unit reaching the end of its current element.
 * @param axis Axis of movement
 * @param direction Direction of movement
 *
 * The player may end up associated with a new base element
 */
void Unit::check_side_limits(Axis axis, Direction direction)
{
	Q_ASSERT(axis == AXIS_X || axis == AXIS_Z);
	Q_ASSERT(direction == DIR_UPW || direction == DIR_DNW);

	double &s = (axis == AXIS_X) ? position.x : position.z;
	bool upwards = (direction == DIR_UPW);
	double threshold = upwards ? 0.5 : -0.5;

	Vec3i r = scene->find_obstacle_candidate(base, position.y, axis, direction);

	if (upwards && s < threshold)
		return;

	if (!upwards && s > threshold)
		return;

	Element *next = scene->neighbour(base, r);
	if (next && !next->is_obstructing()) {
		change_base(next);

	} else {
		Vec3i lower_position = r - Vec3i(0, 1, 0);
		next = scene->neighbour(base, lower_position);
		if (next) {
			change_base(next);
		} else {
			Element *lower = scene->lower_neighbour(base,
								lower_position);
			Q_ASSERT(lower);
			change_base(lower);
		}
	}
}

/**
 * Move unit vertically
 * @param dt Time step
 * @return true if time is consumed
 */
bool Unit::check_vertical_move(gametime dt,
			       const std::vector<Unit *> &obstacles)
{
	double dy = 0.0;

	switch (intent) {
	case GO_UP: dy = climb_speed * dt; break;
	case GO_DOWN: dy = -climb_speed * dt; break;
	default: break;
	}

	// Collision with other units
	if (check_collisions(obstacles))
		return true;

	bool cont;
	if (compare(dy, 0.0) < 0) {
		cont = check_stop_down(dy);
		if (cont) {
			position.y += dy;
			autocenter_x(dt);
			autocenter_z(dt);
			check_bottom_limit();
		} else {
			set_intent(STOP);
			double top = base->get_top(position.x, position.z);
			position.y = compare(position.y, top) >= 0 ?
						top : base->bottom();
		}
		return true;
	}

	if (compare(dy, 0.0) > 0) {
		cont = check_stop_up();
		if (cont) {
			position.y += dy;
			autocenter_x(dt);
			autocenter_z(dt);
			check_top_limit();
		} else {
			set_intent(STOP);
			double top = base->get_top(position.x, position.z);
			position.y = compare(position.y, top) >= 0 ?
						top : base->bottom();
		}
		return true;
	}

	return false;
}

/**
 * Transfer player to the element above, if relevant
 */
void Unit::check_top_limit()
{
	Element *above = scene->neighbour(base, { 0, 1, 0 });
	if (above && compare(position.y, base->get_aabb_top()) >= 0)
		change_base(above);

	// TODO event for reaching top row
}

/**
 * Transfer to the element below, if relevant
 */
void Unit::check_bottom_limit()
{
	if (compare(position.y, base->bottom()) < 0)
		transfer_to_lower_element();
}

/**
 * Check if unit should stop going down
 */
bool Unit::check_stop_down(double dy) const
{
	int vs_top = compare(position.y, base->get_top(position.x, position.z));

	if (vs_top > 0)
		qWarning() << __func__ << "invalid top position";

	if (vs_top == 0) {
		return base->is_crossable_top();
	} else if (vs_top < 0) {
		if (base->is_crossable_bottom() && compare(position.y + dy, base->bottom()) > 0) {
			return true;
		} else {
			const Element *obstacle = scene->neighbour(base, { 0, -1, 0 });
			bool cont = !obstacle || !obstacle->is_walkable_top(this) ||
				obstacle->is_crossable_top();
			return cont;
		}
	}
	return false;
}

/**
 * Check if unit should stop going up
 */
bool Unit::check_stop_up() const
{
	int at_top = compare(position.y, base->get_top(position.x, position.z));
	int at_bottom = compare(position.y, base->bottom());
	Q_ASSERT(at_bottom >= 0);

	// Stop if element doesn't allow to move up
	if (!base->is_crossable_top()) {
		return false;
	}

	// Stop if player is on top and there's nothing above
	if (at_top >= 0) {
		const Element *above = scene->neighbour(base, vec3i::Y1);
		if (!above) {
			return false;
		}
	}

	// Stop if there's an obstacle above
	const Element *obstacle = scene->neighbour(base, vec3i::Y1);
	if (obstacle)
		return !obstacle->is_obstructing_bottom() ||
			obstacle->is_crossable_bottom();

	return true;
}

/**
 * Change base element and update local position
 * @param target New base element
 */
void Unit::change_base(Element *target)
{
	Q_ASSERT(base);

	if (target == nullptr) {
		qDebug() << "Set base to nullptr";
		position = position + base->grid();
		base = nullptr;
		return;
	}

	position = position + base->grid() - target->grid();
	base = target;
	scene->on_unit_base_change(this);
}

/**
 * Return unit's bounding box
 */
Box Unit::get_aabb() const
{
	Vec3 foot = base->coordinates() + position;
	Vec3 p1(foot + Vec3(-0.3, 0.0, -0.3));
	Vec3 p2(foot + Vec3(0.3, 0.8, 0.3));
	return Box(p1, p2);
}

/**
 * Check state of collisions with a set of other units
 * @return true if there is a collision
 */
bool Unit::check_collisions(const std::vector<Unit *> &obstacles)
{
	for (Unit *obstacle : obstacles) {
		if (obstacle == nullptr || obstacle == this) {
			qDebug() << "Assertion failed" << __FILE__ << __LINE__;
			continue;
		}

		if (get_aabb().intersects(obstacle->get_aabb()))
			return true;
	}
	return false;
}

/**
 * Jump out of trap
 */
void Unit::exit_trap(gametime dt)
{
	Q_ASSERT(state == TRAP_OUT);

	auto delta = EXIT_SPEED * dt;
	auto xz = AXIS_X;
	auto dir = DIR_NONE;
	bool exit_found = true;
	Vec3 exit_point;

	switch (intent) {
	case Request::GO_XP:
		exit_point = { 0.5, 0.5, 0.0 };
		xz = AXIS_X;
		dir = DIR_UPW;
		break;
	case Request::GO_XN:
		exit_point = { -0.5, 0.5, 0.0 };
		xz = AXIS_X;
		dir = DIR_DNW;
		break;
	case Request::GO_ZP:
		exit_point = { 0.0, 0.5, 0.5 };
		xz = AXIS_Z;
		dir = DIR_UPW;
		break;
	case Request::GO_ZN:
		exit_point = { 0.0, 0.5, -0.5 };
		xz = AXIS_Z;
		dir = DIR_DNW;
		break;
	default: exit_found = false;
	}

	if (!exit_found)
		return;

	if (position.y < 0.0) {
		autocenter_x(dt);
		autocenter_z(dt);
		position.y += delta;
	} else {
		move_closer(position, exit_point,
				    delta * std::sqrt(2.0) / 2.0);
	}

	if (equality(position, exit_point)) {
		position = exit_point;
		state = NORMAL;
		base->unblock();
		check_side_limits(xz, dir);
	}
}

/**
 * Move unit according to base element's field, if any.
 * @param dt Time step
 */
bool Unit::check_field(gametime dt)
{
	if (!base->has_field())
		return false;

	Vec3 delta = base->get_field_velocity() * dt;

	if (delta.is_null())
		return false;

	bool vertical = equality(delta.x, 0) && equality(delta.z, 0);

	if (vertical) {
		// TODO check top (aabb or not)
		if (delta.y > 0 && equality(position.y, base->get_aabb_top())) {
			check_top_limit();
			return false;
		}

		// TODO check top (aabb or not)
		if (position.y > base->get_aabb_top())
			return false;

		if (position.y < base->bottom() && !equality(position.y,
							     base->bottom()))
			return false;

		autocenter_x(dt);
		autocenter_z(dt);
	}

	position = position + delta;

	if (vertical) {
		check_top_limit();
	} else {
		if (!equality(delta.x, 0.0)) {
			auto dir = delta.x > 0.0 ? DIR_UPW : DIR_DNW;
			check_side_limits(AXIS_X, dir);
		}
		if (!equality(delta.z, 0.0)) {
			auto dir = delta.z > 0.0 ? DIR_UPW : DIR_DNW;
			check_side_limits(AXIS_Z, dir);
		}
	}

	return true;
}

/**
 * Slide along a slope if the unit is on such an element
 * @param dt remaining time
 * @return true if time is consumed
 */
bool Unit::check_sliding(gametime dt)
{
	if (!base->is_slider())
		return false;

	double top = base->get_top(position.x, position.z);
	if (!equality(top, position.y)) {
		qDebug() << "Warning: unit should slide but is above top";
		return false;
	}

	if (position.y < top) {
		qDebug() << "Warning: unit inside slider";
	}

	Vec3 target = base->get_exit_point();
	if (equality(position, target)) {
		qDebug() << "Warning: sliding but already on target";
		return false;
	}

	// Actual move
	move_closer(position, target, SLIDE_HSPEED * dt);
	position.y = base->get_top(position.x, position.z);

	// If not yet on target, nothing else to do
	if (!equality(position, target)) {
		return true;
	}

	// Otherwise, handle transfer to neighbour element.
	// Here we assume exit points are on one axis
	if (equality(position.z, 0.0)) {
		auto dir = position.x > 0.0 ? DIR_UPW : DIR_DNW;
		check_side_limits(AXIS_X, dir);
	} else if (equality(position.x, 0.0)) {
		auto dir = position.z > 0.0 ? DIR_UPW : DIR_DNW;
		check_side_limits(AXIS_Z, dir);
	} else {
		qDebug() << "Warning: slider exit point not on axis";
	}
	return true;
}
