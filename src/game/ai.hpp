/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AI_HPP
#define AI_HPP

#include "element.hpp"
#include "unit.hpp"
#include "vec.hpp"

#include <list>
#include <map>
#include <set>
#include <vector>

class Scene;

/**
 * Logical location in game world
 */
struct GraphLocation {
	const Element *element;
	bool top;
};

/**
 * Element of graph used to compute shortest path between enemy and player.
 */
struct GraphNode
{
	Vec3i position;
	const Element *element;
	bool top;
	GraphNode *xp = nullptr;
	GraphNode *xn = nullptr;
	GraphNode *yp = nullptr;
	GraphNode *yn = nullptr;
	GraphNode *zp = nullptr;
	GraphNode *zn = nullptr;
};

/**
 * Path Finder
 */
class PathFinder
{
public:
	PathFinder(Scene &);
	~PathFinder();

	Unit::Request get_intent(Vec3 position) const;
	void set_current(const Element *, const Vec3 &position);
	void set_target(const Element *, const Vec3 &position);

	std::vector<Vec3> get_path() const;
	std::vector<Vec3> get_graph() const;

private:
	static double heuristic_cost(const Vec3i &u, const Vec3i &v);

	static std::list<GraphNode *>
	reconstruct(const std::map<GraphNode *, GraphNode *> &, GraphNode *);

	void init_graph();
	void free_graph();

	std::list<GraphNode *> compute_path();

	void connect_node(GraphNode *, Axis, Direction);
	void connect_node_down(GraphNode *);
	void connect_node_up(GraphNode *);

	GraphNode *find_node(const Element *element, bool top_first) const;
	GraphNode *find_node(Vec3i position) const;
	GraphNode *find_node(GraphLocation) const;
	std::set<GraphNode *> find_neighbours(const GraphNode *) const;

	Scene &scene;

	std::vector<GraphNode *> graph;
	std::list<GraphNode *> path;

	GraphLocation current_location = { nullptr, false };
	GraphLocation target_location = { nullptr, false };
	GraphLocation last_valid_target = { nullptr, false };
};

#endif // AI_HPP
