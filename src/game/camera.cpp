/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "camera.hpp"

#include "helper.hpp"

#include <QDebug>
#include <QJsonArray>

#include <cmath>

using helper::equality;

const double CAMERA_ACCEL = 0.0001;

/**
 * Constructor
 */
Camera::Camera()
{
	clear();
}

/**
 * Clear. Remove all locations.
 */
void Camera::clear()
{
	locations.clear();
	preferred = 0;
	position_speed.clear();
	target_speed.clear();
	focus.clear();

	current.target.clear();
	current.position = { 0.0, 0.0, 10.0 };
	current.weight = 0.0;
}

/**
 * Add a camera location.
 * If this is the first one, it is also used to set the current location.
 */
void Camera::add_location(int id, CameraLocation location)
{
	if (location.weight < 0.0)
		location.weight = 0.0;

	if (location.weight > 1.0)
		location.weight = 1.0;

	if (locations.empty())
		current = location;

	locations[id] = location;
}

/**
 * Get camera current position
 */
Vec3 Camera::position() const
{
	return current.position;
}

/**
 * Get camera current target
 */
Vec3 Camera::target() const
{
	return current.target;
}

/**
 * Select camera id
 * @param id
 */
void Camera::select(int id)
{
	if (id != preferred && locations.find(id) == locations.end())
		qDebug() << "Warning: unknown camera" << id;

	preferred = id;
}

/**
 * Update camera system
 * @param dt Time step
 */
void Camera::update(gametime dt)
{
	auto it = locations.find(preferred);

	if (it == locations.end())
		return;

	CameraLocation goal = it->second;
	Vec3 target = goal.target * (1.0 - goal.weight) + focus * goal.weight;

	update_location(current.position, position_speed, goal.position, dt);
	update_location(current.target, target_speed, target, dt);
}

/**
 * Update camera speed and location
 * @param location Position to move
 * @param speed Speed of position vector, will be modified too
 * @param goal New position as goal
 * @param dt Time step
 */
void Camera::update_location(Vec3 &location, Vec3 &speed,
			     const Vec3 &goal, gametime dt)
{
	Vec3 v = goal - location;

	if (v.is_null())
		return;

	// Default acceleration towards goal
	Vec3 acceleration = v.normalized() * CAMERA_ACCEL;
	Vec3 new_speed = speed + acceleration * dt;

	// Radius of sphere inside which we need to slow down
	double radius = new_speed.length2() / (2 * CAMERA_ACCEL);

	// Far enough to continue acceleration...
	if (v.length2() > radius * radius) {
		Vec3 new_position = location + new_speed * dt;
		// But only if we're moving closer to the target
		if ((goal - new_position).length2() < v.length2()) {
			speed = new_speed;
			location = new_position;
			return;
		}
	}

	// Default deceleration
	Vec3 deceleration = speed.normalized() * CAMERA_ACCEL * dt;

	// Close enough to stop
	if (speed.length2() < deceleration.length2()) {
		speed.clear();
		location = goal;
		return;
	}

	// Decelerate
	speed = speed - deceleration;
	location = location + speed * dt;
}

/**
 * Return view vector
 */
Vec3 Camera::view_vector() const
{
	return current.target - current.position;
}

/**
 * Export cameras to JSON format
 *
 * @param array JSON array object to export to
 */
void Camera::write(QJsonArray &array) const
{
	for (auto entry : locations) {
		int id = entry.first;
		CameraLocation location = entry.second;

		QJsonObject json;
		json["id"] = id;

		QJsonObject position;
		location.position.write(position);
		json["position"] = position;

		QJsonObject target;
		location.target.write(target);
		json["target"] = target;

		json["weight"] = location.weight;

		array.append(json);
	}
}

/**
 * Import from JSON format
 *
 * @param json JSON object to import
 */
void Camera::read(const QJsonArray &array)
{
	clear();

	for (int i = 0; i < array.size(); ++i) {
		CameraLocation location;
		QJsonObject json = array[i].toObject();
		int id = json["id"].toInt();
		location.position.read(json["position"].toObject());
		location.target.read(json["target"].toObject());
		location.weight = json["weight"].toDouble();
		add_location(id, location);
	}
}

/**
 * Set the camera focus location
 */
void Camera::set_focus(const Vec3 &vec)
{
	focus = vec;
}
