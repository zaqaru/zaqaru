/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ai.hpp"

#include "helper.hpp"
#include "scene.hpp"

#include <QDebug>
#include <QtGlobal>

#include <map>
#include <set>
#include <sstream>

using helper::compare;

bool operator==(const GraphLocation &lhs, const GraphLocation &rhs);
bool operator!=(const GraphLocation &lhs, const GraphLocation &rhs);

static void add_segment(std::vector<Vec3> &, const Vec3 &, GraphNode *);

// ---------------------------------------------------------------------------
// GraphLocation

bool operator==(const GraphLocation &lhs, const GraphLocation &rhs)
{
	return lhs.element == rhs.element && lhs.top == rhs.top;
}

bool operator!=(const GraphLocation &lhs, const GraphLocation &rhs)
{
	return !(lhs == rhs);
}

// ---------------------------------------------------------------------------
// PathFinder

/**
 * Constructor
 */
PathFinder::PathFinder(Scene &world)
	: scene(world)
{
	init_graph();
}

/**
 * Destructor
 */
PathFinder::~PathFinder()
{
	free_graph();
}

/**
 * Create level graph for usage by AI (vertices and edges to compute paths).
 */
void PathFinder::init_graph()
{
	free_graph();

	// TODO: we create 1 graph for each guard while one per kind
	// of enemy (only one for now) would be enough.

	std::vector<Element *> elements = scene.get_elements();

	// Retrieve the low nodes (one per element)
	for (const Element *element : elements) {
		GraphNode *node = new GraphNode;
		graph.push_back(node);
		node->element = element;
		node->position = element->grid();
		node->top = false;
	}

	// Retrieve the top nodes when there is nothing above
	for (const Element *element : elements) {
		if (!element->is_walkable_top())
			continue;

		GraphNode *above = find_node(element->grid() + vec3i::Y1);
		if (!above) {
			GraphNode *node = new GraphNode;
			graph.push_back(node);
			node->element = element;
			node->position = element->grid() + vec3i::Y1;
			node->top = true;
		}
	}

	// Link to other nodes
	for (auto node : graph) {
		connect_node(node, AXIS_X, DIR_UPW);
		connect_node(node, AXIS_X, DIR_DNW);
		connect_node(node, AXIS_Z, DIR_UPW);
		connect_node(node, AXIS_Z, DIR_DNW);
		connect_node_up(node);
		connect_node_down(node);
	}
}

/**
 * Free structure containing the graph
 */
void PathFinder::free_graph()
{
	for (GraphNode *node : graph)
		delete node;

	graph.clear();
	path.clear();
}

/**
 * Connect node in a specific horizontal direction, if relevant
 */
void PathFinder::connect_node(GraphNode *node, Axis xz, Direction dir)
{
	Q_ASSERT(node);
	Q_ASSERT(xz == AXIS_X || xz == AXIS_Z);
	Q_ASSERT(dir == DIR_UPW || dir == DIR_DNW);

	// Cases to reject
	if (!node->top) {
		if (!node->element->is_walkable_bottom()) {
			auto position_below = node->position + Vec3i(0, -1, 0);
			GraphNode *below = find_node(position_below);
			if (!below || !below->element->is_walkable_top())
				return;
		}
		if (!node->element->is_crossable(xz, dir))
			return;
	} else {
		if (!node->element->is_walkable_top(xz, dir))
			return;
	}

	int rx = xz == AXIS_Z ? 0 : (dir == DIR_UPW ? 1 : -1);
	int rz = xz == AXIS_X ? 0 : (dir == DIR_UPW ? 1 : -1);

	GraphNode **target = (xz == AXIS_X) ?
		(dir == DIR_UPW ? &node->xp : &node->xn) :
		(dir == DIR_UPW ? &node->zp : &node->zn);

	Vec3i next_position(rx, 0, rz);
	GraphNode *next = find_node(node->position + next_position);

	if (next) {
		if (next->top || !next->element->is_obstructing(false))
			*target = next;
	} else {
		const Element *lower = scene.lower_neighbour(node->element,
							     next_position);
		if (lower)
			*target = find_node(lower, true);
	}
}

/**
 * Connect node to the node above, if relevant
 */
void PathFinder::connect_node_up(GraphNode *node)
{
	Q_ASSERT(node);

	if (node->top)
		return;

	if (!node->element->is_climbable() || !node->element->is_crossable_top())
		return;

	GraphNode *above = find_node(node->position + vec3i::Y1);

	if (above) {
		if (above->element->is_obstructing_bottom(false))
			return;

		node->yp = above;
	} else {
		qDebug() << "Unexpected" << __FILE__ << __LINE__;
	}
}

/**
 * Connect node to a node below, if relevant
 */
void PathFinder::connect_node_down(GraphNode *node)
{
	Q_ASSERT(node);

	const Element *e = node->element;

	if (node->top) {
		if (!e->is_walkable_top() || e->is_climbable())
			node->yn = find_node(e, false);
		return;
	}

	if (!e->is_crossable_bottom())
		return;

	const Element *lower = scene.lower_neighbour(e, Vec3i(0, -1, 0));
	if (!lower)
		return;

	if (lower->grid_y() == node->position.y - 1 &&
	    lower->is_obstructing_top(false))
		return;

	node->yn = find_node(lower, true);
}

/**
 * Find a specific node in the current graph
 */
GraphNode *PathFinder::find_node(GraphLocation loc) const
{
	for (GraphNode *node : graph)
		if (node->element == loc.element && node->top == loc.top)
			return node;

	return nullptr;
}

/**
 * Find a specific node in the current graph
 */
GraphNode *PathFinder::find_node(Vec3i position) const
{
	for (GraphNode *node : graph)
		if (node->position == position)
			return node;

	return nullptr;
}

/**
 * Find a specific node in the current graph
 * @param elem Element associated with node to look for
 * @param top_first If 2 nodes match with element, choose top
 */
GraphNode *PathFinder::find_node(const Element *element, bool top_first) const
{
	GraphNode *top = nullptr;
	GraphNode *inside = nullptr;

	for (GraphNode *node : graph) {
		if (node->element == element) {
			if (node->top)
				top = node;
			else
				inside = node;
		}
	}

	if (top_first)
		return top != nullptr ? top : inside;
	else
		return inside != nullptr ? inside : top;
}

/**
 * Find neighbours of a node in the graph.
 * This is the list of all the nodes directly accessible from a specific one.
 */
std::set<GraphNode *> PathFinder::find_neighbours(const GraphNode *node) const
{
	std::set<GraphNode *> results;
	auto it = std::find(graph.begin(), graph.end(), node);

	if (it == graph.end())
		return results;

	GraphNode *p = *it;

	if (p->xp)
		results.insert(p->xp);
	if (p->xn)
		results.insert(p->xn);
	if (p->yp)
		results.insert(p->yp);
	if (p->yn)
		results.insert(p->yn);
	if (p->zp)
		results.insert(p->zp);
	if (p->zn)
		results.insert(p->zn);

	return results;
}

/**
 * Set origin for pathfinding
 */
void PathFinder::set_current(const Element *element, const Vec3 &position)
{
	Q_ASSERT(element);
	GraphNode *node = nullptr;

	Vec3i p = element->grid();

	if (position.y >= 0.5)
		node = find_node(p + vec3i::Y1);

	if (node == nullptr) {
		bool top_first = compare(position.y, 0.0) >= 0;
		node = find_node(element, top_first);
	}

	if (node == nullptr) {
		qDebug() << "Unexpected" << __func__ << __LINE__;
		return;
	}

	// Workaround to prevent guards from prematurely changing
	// direction horizontally and being stuck by an obstacle
	if (compare(position.y, 0.0) > 0 && compare(position.y, 0.5) < 0)
		return;

	GraphLocation current { node->element, node->top };
	bool recompute = current_location != current;
	current_location = current;

	if (recompute) {
		path = compute_path();
		if (!path.empty())
			last_valid_target = target_location;
	}
}

/**
 * Set target for pathfinding
 */
void PathFinder::set_target(const Element *element, const Vec3 &position)
{
	Q_ASSERT(element);
	bool top_first = compare(position.y, 0.0) >= 0;
	GraphNode *node = find_node(element, top_first);
	if (node == nullptr) {
		auto p = element->grid();
		qDebug() << "Assertion failed" << __FILE__ << __LINE__
			 << p.x << p.y << p.z << top_first;
		return;
	}

	GraphLocation target { node->element, node->top };
	bool recompute = target_location != target;
	target_location = target;

	if (recompute) {
		path = compute_path();
		if (!path.empty()) {
			last_valid_target = target_location;
		} else {
			if (last_valid_target.element) {
				target_location = last_valid_target;
				path = compute_path();
			}
		}
	}
}

/**
 * Compute shortest path (with A* algorithm)
 *
 * @return Path as list from target to origin (origin excluded)
 */
std::list<GraphNode *> PathFinder::compute_path()
{
	// Cancel if not fully initialized
	if (!current_location.element || !target_location.element)
		return std::list<GraphNode *>();

	GraphNode *start = find_node(current_location);
	Q_ASSERT(start);

	auto goal = find_node(target_location);
	Q_ASSERT(goal);

	std::set<GraphNode *> closed_set;
	std::set<GraphNode *> open_set;
	std::map<GraphNode *, GraphNode *> from;

	open_set.insert(start);

	std::map<GraphNode *, double> g;
	std::map<GraphNode *, double> f;
	g[start] = 0;
	f[start] = g[start] + heuristic_cost(start->position, goal->position);

	while (!open_set.empty()) {
		// Find node with lowest f score
		auto current = *open_set.begin();
		Q_ASSERT(f.find(current) != f.end());
		auto score = f[current];
		for (auto node : open_set) {
			Q_ASSERT(f.find(node) != f.end());
			if (f[node] < score) {
				current = node;
				score = f[node];
			}
		}

		if (current == goal) {
			auto result = reconstruct(from, goal);
			Q_ASSERT(!result.empty());
			result.pop_back(); // remove the current node
			return result;
		}

		// Remove current from the open set, add it to closed set
		size_t n = open_set.erase(current);
		Q_ASSERT(n == 1);
		closed_set.insert(current);

		std::set<GraphNode *> neighbours = find_neighbours(current);
		for (auto neighbour : neighbours) {
			if (closed_set.find(neighbour) != closed_set.end())
				continue;

			double distance = 1.0; // current-neighbour distance
			auto g_candidate = g[current] + distance;

			auto it = g.find(neighbour);
			if (open_set.find(neighbour) == open_set.end() ||
			    (it != g.end() && g_candidate < it->second)) {

				if (open_set.find(neighbour) == open_set.end())
					open_set.insert(neighbour);

				from[neighbour] = current;
				g[neighbour] = g_candidate;
				f[neighbour] = g[neighbour] +
					heuristic_cost(neighbour->position,
						goal->position);
			}
		}
	}

	// No path found
	return std::list<GraphNode *>();
}

/**
 * Heuristic cost of movement between two positions
 */
double PathFinder::heuristic_cost(const Vec3i &u, const Vec3i &v)
{
	return Vec3(u - v).length();
}

/**
 * Reconstruct path from target to origin
 */
std::list<GraphNode *>
PathFinder::reconstruct(const std::map<GraphNode *, GraphNode *> &from,
	    GraphNode *current)
{
	std::list<GraphNode *> p;

	auto it = from.find(current);
	if (it != from.end())
		p = reconstruct(from, it->second);

	p.push_front(current);
	return p;
}

/**
 * Provide the direction to follow for the next move
 * @param position Current relative position
 */
Unit::Request PathFinder::get_intent(Vec3 position) const
{
	if (current_location == target_location)
		return Unit::NONE;

	if (path.empty())
		return Unit::NONE;

	GraphNode *node = path.back();
	GraphNode *current = find_node(current_location);
	Q_ASSERT(current != nullptr);

	Unit::Request request = Unit::NONE;

	if (current->xp == node)
		request = Unit::GO_XP;
	else if (current->xn == node)
		request = Unit::GO_XN;
	else if (current->zp == node)
		request = Unit::GO_ZP;
	else if (current->zn == node)
		request = Unit::GO_ZN;
	else if (current->yp == node)
		request = Unit::GO_UP;
	else if (current->yn == node)
		request = Unit::GO_DOWN;

	if (request == Unit::NONE)
		qDebug() << "Could not find expected node in AI graph";

	// TODO this is a workaround for VFIELD. This is needed to have
	// the guard go down first before attempting to go on the sides of
	// a VFIELD (could be blocked by an obstructing element otherwise).
	if (compare(position.y, 0.0) >= 0 && compare(position.y, 0.5) < 0 &&
	    (request == Unit::GO_XP || request == Unit::GO_XN ||
	     request == Unit::GO_ZP || request == Unit::GO_ZN))
		request = Unit::GO_DOWN;

	return request;
}

/**
 * Return a list of vertices describing the current shortest path.
 */
std::vector<Vec3> PathFinder::get_path() const
{
	std::vector<Vec3> result;
	result.reserve(path.size());

	for (GraphNode *node : path) {
		Vec3 v(node->position);
		result.push_back(v - Vec3(0.1, 0.1, 0.1));
	}
	return result;
}

/**
 * Return a list of segments describing the path finding graph.
 *
 * The returned vertices should be paired to get the segments.
 */
std::vector<Vec3> PathFinder::get_graph() const
{
	std::vector<Vec3> result;
	result.reserve(graph.size());

	for (GraphNode *node : graph) {
		Vec3 v(node->position);
		Vec3 p(v);
		Vec3 r(p);
		r.y -= 0.1;
		result.push_back(p);
		result.push_back(r);

		add_segment(result, p, node->xn);
		add_segment(result, p, node->xp);
		add_segment(result, p, node->yn);
		add_segment(result, p, node->yp);
		add_segment(result, p, node->zn);
		add_segment(result, p, node->zp);
	}
	return result;
}

/**
 * Add segment to the list of vertices.
 * @param segments Target list.
 * @param p First location of new segment.
 * @param node Node carrying second location of new segment.
 */
void add_segment(std::vector<Vec3> &segments, const Vec3 &p, GraphNode *node)
{
	if (node == nullptr)
		return;

	segments.push_back(p);
	Vec3 q(node->position);
	segments.push_back(q);
}
