/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vec.hpp"

#include "helper.hpp"

#include <QDebug>

#include <cmath>
#include <sstream>

using helper::equality;

// ---------------------------------------------------------------------------
// Vec3

Vec3::Vec3()
	: x(0.0), y(0.0), z(0.0)
{
}

Vec3::Vec3(double px, double py, double pz)
	: x(px), y(py), z(pz)
{
}

Vec3::Vec3(double azimuth, double inclination)
	: x(std::sin(inclination) * std::sin(azimuth)),
	  y(std::cos(inclination)),
	  z(std::sin(inclination) * std::cos(azimuth))
{
}

Vec3::Vec3(const Vec3i &v)
	: x(v.x), y(v.y), z(v.z)
{
}

void Vec3::clear()
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

/**
 * Rotate vector around vertical axis
 *
 * Only 90-degree rotations are considered
 */
Vec3 Vec3::rotated(int angle) const
{
	angle %= 360;

	if (angle < 0)
		angle += 360;

	Q_ASSERT(angle >= 0 && angle < 360);

	Vec3 v(*this);

	while (angle > 0) {
		double tmp = v.z;
		v.z = -v.x;
		v.x = tmp;
		angle -= 90;
	}

	if (angle != 0)
		qDebug() << __FILE__ ":" << __LINE__ << ":Unexpected rotation";

	return v;
}

Vec3 Vec3::normalized() const
{
	if (is_null())
		return Vec3();

	return *this / length();
}

void Vec3::set_length(double length)
{
	*this = normalized() * length;
}

std::string Vec3::to_string() const
{
	std::stringstream s;
	s << "[" << x << ":" << y << ":" << z << "]";
	return s.str();
}

/**
 * Return dot product with another vector
 */
double Vec3::dot(const Vec3 &v) const
{
	return x * v.x + y * v.y + z * v.z;
}

double Vec3::length2() const
{
	return x * x + y * y + z * z;
}

double Vec3::length() const
{
	return std::sqrt(length2());
}

bool Vec3::is_null() const
{
	return equality(x, 0.0) && equality(y, 0.0) && equality(z, 0.0);
}

/**
 * Export element to JSON format
 *
 * @param json JSON object to export to
 */
void Vec3::write(QJsonObject &json) const
{
	json["x"] = x;
	json["y"] = y;
	json["z"] = z;
}

/**
 * Import from JSON format
 *
 * @param json JSON object to import
 */
void Vec3::read(const QJsonObject &json)
{
	clear();

	// Invalid types/values will default to zero
	x = json["x"].toDouble();
	y = json["y"].toDouble();
	z = json["z"].toDouble();
}

Vec3 create_vec3(Axis axis, Direction direction)
{
	Vec3 v;

	if (direction == DIR_NONE)
		return v;

	int sign = direction == DIR_UPW ? 1 : -1;

	switch (axis) {
	case AXIS_X:
		v.x = 1.0 * sign;
		break;
	case AXIS_Y:
		v.y = 1.0 * sign;
		break;
	case AXIS_Z:
		v.z = 1.0 * sign;
		break;
	default:
		Q_ASSERT(false);
	}

	return v;
}

// ---------------------------------------------------------------------------
// Vec2i

Vec2i::Vec2i()
	: x(0), y(0)
{
}

Vec2i::Vec2i(int px, int py)
	: x(px), y(py)
{
}

// ---------------------------------------------------------------------------
// Vec3i

Vec3i::Vec3i()
	: x(0), y(0), z(0)
{
}

Vec3i::Vec3i(int px, int py, int pz)
	: x(px), y(py), z(pz)
{
}

Vec3i::Vec3i(const Vec2i &v)
	: x(v.x), y(v.y), z(0)
{
}

Vec3i::Vec3i(const Vec3 &v)
	: x(std::round(v.x)), y(std::round(v.y)), z(std::round(v.z))
{
}

void Vec3i::clear()
{
	x = 0;
	y = 0;
	z = 0;
}

/**
 * Export to JSON format
 *
 * @param json JSON object to export to
 */
void Vec3i::write(QJsonObject &json) const
{
	json["x"] = x;
	json["y"] = y;
	json["z"] = z;
}

/**
 * Import from JSON format
 *
 * @param json JSON object to import
 */
void Vec3i::read(const QJsonObject &json)
{
	clear();

	// Invalid types/values will default to zero
	x = json["x"].toInt();
	y = json["y"].toInt();
	z = json["z"].toInt();
}

// ---------------------------------------------------------------------------
// Operator <

bool operator<(const Vec2i &lhs, const Vec2i &rhs)
{
	if (lhs.y < rhs.y)
		return true;

	if (lhs.y > rhs.y)
		return false;

	return lhs.x < rhs.x;
}

bool operator<(const Vec3i &lhs, const Vec3i &rhs)
{
	if (lhs.z < rhs.z)
		return true;

	if (lhs.z > rhs.z)
		return false;

	if (lhs.y < rhs.y)
		return true;

	if (lhs.y > rhs.y)
		return false;

	return lhs.x < rhs.x;
}

// ---------------------------------------------------------------------------
// Operator ==

bool operator==(const Vec3i &u, const Vec3i &v)
{
	return u.x == v.x && u.y == v.y && u.z == v.z;
}

// ---------------------------------------------------------------------------
// Operator +

Vec2i operator+(const Vec2i &u, const Vec2i &v)
{
	return Vec2i(u.x + v.x, u.y + v.y);
}

Vec3i operator+(const Vec3i &u, const Vec3i &v)
{
	return Vec3i(u.x + v.x, u.y + v.y, u.z + v.z);
}

Vec3 operator+(const Vec3 &a, const Vec3 &b)
{
	return Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vec3 operator+(const Vec3 &a, const Vec3i &b)
{
	return Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}

// ---------------------------------------------------------------------------
// Operator -

Vec3i operator-(const Vec3i &u, const Vec3i &v)
{
	return Vec3i(u.x - v.x, u.y - v.y, u.z - v.z);
}

Vec3 operator-(const Vec3 &a, const Vec3 &b)
{
	return Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}

Vec3 operator-(const Vec3 &a, const Vec3i &b)
{
	return Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}

// ---------------------------------------------------------------------------
// Operator *

Vec3 operator*(const Vec3 &v, unsigned int a)
{
	return Vec3(v.x * a, v.y * a, v.z * a);
}

Vec3 operator*(const Vec3 &v, double a)
{
	return Vec3(v.x * a, v.y * a, v.z * a);
}

// ---------------------------------------------------------------------------
// Operator /

Vec3 operator/(const Vec3 &v, double a)
{
	return Vec3(v.x / a, v.y / a, v.z / a);
}
