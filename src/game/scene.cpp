/*
 * Zaqaru
 * Copyright © 2018, 2019, 2020 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "scene.hpp"

#include "helper.hpp"

#include <QDebug>
#include <QJsonArray>

#include <cmath>

using helper::compare;

/**
 * Constructor
 */
Scene::Scene()
	: player(this, Vec3())
{
}

/**
 * Destructor
 */
Scene::~Scene()
{
	clear();
}

/**
 * Add hardcoded elements to create a test scene.
 */
void Scene::setup_test_level()
{
	elements.clear();

	std::set<Element::Property> unlock_property;
	unlock_property.insert(Element::APPEAR_ON_UNLOCK);

	fill_elements(Element::BLOCK, { 0, 0, 0 }, { 19, 0, 0 }, 0);
	fill_elements(Element::BLOCK, { 0, 10, 0 }, { 18, 10, 0 });
	fill_elements(Element::VFIELD, { 15, 1, 0 }, { 15, 10, 0 });
	fill_elements(Element::VFIELD, { 19, 1, 0 }, { 19, 22, 0 },
		      0, 0, unlock_property);

	fill_elements(Element::BLOCK, { 2, 6, 0 }, { 13, 8, 0 }, 6);
	fill_elements(Element::BLOCK, { 2, 3, 0 }, { 13, 4, 0 }, 7);

	add_diagonal(Element::SLOPE, { 18, 10, 1 }, { 18, 2, 9 }, 2 );
	add_diagonal(Element::USLOPE, { 18, 9, 1 }, { 18, 1, 9 }, 2);
	fill_elements(Element::BLOCK, { 2, 1, 10 }, { 18, 1, 10 }, 2);
	fill_elements(Element::SLOPE, { 1, 1, 10 }, { 1, 1, 10 }, 1, 270);

	fill_elements(Element::HSTREAM, { 0, 1, 10 }, { 0, 1, 1 }, 1, 90);
	fill_elements(Element::HSTREAM, { 0, 1, -1 }, { 0, 1, -10 }, 1, 270);
	fill_elements(Element::BLOCK, { 0, 10, 10 }, { 0, 10, -9 }, 1);
	fill_elements(Element::HSTREAM, { 0, 10, -10 }, { 0, 10, -10 }, 8);
	fill_elements(Element::BLOCK, { 0, 15, 10 }, { 0, 15, -4 }, 1);
	fill_elements(Element::HFIELD, { 0, 16, -5 }, { 0, 16, -5 }, 1);
	fill_elements(Element::BLOCK, { 0, 15, -6 }, { 0, 15, -9 }, 1);
	fill_elements(Element::TRAP, { 0, 15, -10 }, { 0, 15, -10 }, 3);
	fill_elements(Element::BLOCK, { -1, 10, -10 }, { -1, 15, -10 }, 1 );
	fill_elements(Element::USTREAM, { 0, 1, 0 }, { 0, 15, 0 }, 1);

	fill_elements(Element::TOUGH, { 1, 16, 0 }, { 1, 16, 0 }, 1);
	fill_elements(Element::USLOPE, { 1, 15, 0 }, { 1, 15, 0 }, 1, 270);
	fill_elements(Element::TOUGH, { 1, 16, 10 }, { 1, 16, 10 }, 1);
	fill_elements(Element::USLOPE, { 1, 15, 10 }, { 1, 15, 10 }, 1, 270);
	fill_elements(Element::TOUGH, { 1, 16, -10 }, { 1, 16, -10 }, 1);
	fill_elements(Element::USLOPE, { 1, 15, -10 }, { 1, 15, -10 }, 1, 270);

	add_diagonal(Element::SLOPE, { 0, 13, -10 }, { 11, 2, -10 }, 3, 90 );
	add_diagonal(Element::USLOPE, { 0, 12, -10 }, { 10, 2, -10 }, 3, 90 );

	fill_elements(Element::BLOCK, { 1, 1, -10 }, { 11, 1, -10 }, 8);
	fill_elements(Element::BLOCK, { 1, 0, -10 }, { 18, 0, -10 }, 3);
	fill_elements(Element::USTREAM, { 18, 1, -10 }, { 18, 21, -10 }, 4);
	add_diagonal(Element::SLOPE, { 18, 21, -9 }, { 18, 12, 0 }, 5 );
	add_diagonal(Element::USLOPE, { 18, 20, -9 }, { 18, 12, -1 }, 5 );
	fill_elements(Element::HSTREAM, { 9, 15, -1 }, { 9, 15, -9 }, 2, 270 );
	fill_elements(Element::USTREAM, { 9, 0, -9 }, { 9, 14, -9}, 9);

	fill_elements(Element::BLOCK, { 12, 12, 0 }, { 15, 12, 0 });
	fill_elements(Element::BLOCK, { 12, 15, 0 }, { 15, 15, 0 });
	fill_elements(Element::VFIELD, { 11, 11, 0 }, { 11, 15, 0 });
	fill_elements(Element::VFIELD, { 16, 11, 0 }, { 16, 15, 0 });
	fill_elements(Element::TRAP, { 14, 12, 0 }, { 14, 15, 0 });

	add_object(Object::PLAYER, { 3, 11, 0 });
	add_object(Object::GUARD, { 17, 1, 0 });
	add_object(Object::GUARD, { 13, 16, 0 });
	add_object(Object::GUARD, { 6, 9, 0 });
	add_object(Object::GUARD, { 4, 1, 0 });

	add_object(Object::COIN, { 5, 1, 0 });
	add_object(Object::COIN, { 5, 2, 0 });
	add_object(Object::COIN, { 10, 1, 0 });
	add_object(Object::COIN, { 10, 2, 0 });
	add_object(Object::COIN, { 7, 5, 0 });
	add_object(Object::COIN, { 9, 9, 0 });

	add_object(Object::COIN, { 18, 5, 7 });
	add_object(Object::COIN, { 4, 2, 10 });
	add_object(Object::COIN, { 10, 2, 10 });
	add_object(Object::COIN, { 16, 2, 10 });

	add_object(Object::COIN, { 0, 11, 8 });
	add_object(Object::COIN, { 0, 11, -8 });
	add_object(Object::COIN, { 0, 16, 7 });
	add_object(Object::COIN, { 0, 16, -7 });
	add_object(Object::COIN, { 0, 16, 4 });
	add_object(Object::COIN, { 0, 16, -4 });
	add_object(Object::COIN, { 0, 16, -10 });

	add_object(Object::COIN, { 9, 2, -10 });
	add_object(Object::COIN, { 16, 1, -10 });

	add_object(Object::COIN, { 12, 16, 0 });
	add_object(Object::COIN, { 15, 16, 0 });

	add_object(Object::RESPAWN, { 1, 22, 0 });
	add_object(Object::RESPAWN, { 1, 22, 10 });
	add_object(Object::RESPAWN, { 1, 22, -10 });

	add_camera(0, {  9, 10, 20 }, { 9, 10, 0 });
	add_camera(1, { -18, 15, 0 }, { 0, 10, 0 });
	add_camera(2, { 13, 4, 26 }, { 9, 10, 0 });
	add_camera(3, { 16, 6, -28 }, { 7, 10, 0 });
	add_camera(4, { 30, 25, -10 }, { 9, 18, 0 }, 0.4);
	add_camera(5, { 28, 18, 3 }, { 1, 14, 0 }, 0.8);
	add_camera(6, {  8, 12, 8 }, { 7, 7, 0 });
	add_camera(7, {  7, 5, 11 }, { 7, 3, 0 });
	add_camera(8, {  9, 5, -27 }, { 9, 10, 0 });
	add_camera(9, { -5, 12, -4 }, { 18, 10, -4 }, 0.6);
	add_camera(10, {  6, 12, 12 }, { 7, 5, 0 });

	add_light(Vec3(10,5,5));
	add_light(Vec3(15,15,-5));
}

/**
 * Fill space with elements
 */
void Scene::fill_elements(Element::Type type, Vec3i u, Vec3i v,
			  int camera_id, int angle,
			  const std::set<Element::Property> &properties)
{
	int dx = u.x <= v.x ? 1 : -1;
	int dy = u.y <= v.y ? 1 : -1;
	int dz = u.z <= v.z ? 1 : -1;

	for (int x = u.x; x != v.x + dx; x += dx) {
		for (int y = u.y; y != v.y + dy; y += dy) {
			for (int z = u.z; z != v.z + dz; z += dz) {
				Vec3i position(x, y, z);
				auto e = new Element(this, type, position);
				for (auto property : properties)
					e->set_property(property);
				e->set_camera_id(camera_id);
				e->set_angle(angle);
				add_element(e);
			}
		}
	}
}

/**
 * Fill space with elements
 */
void Scene::fill_elements(Element::Type type, Vec3i u, Vec3i v,
			  int camera_id, int angle)
{
	std::set<Element::Property> properties;
	fill_elements(type, u, v, camera_id, angle, properties);
}

/**
 * Fill space with elements
 */
void Scene::add_diagonal(Element::Type type, Vec3i u, Vec3i v,
			  int camera_id, int angle,
			  const std::set<Element::Property> &properties)
{
	int dx = u.x <= v.x ? 1 : -1;
	int dy = u.y <= v.y ? 1 : -1;
	int dz = u.z <= v.z ? 1 : -1;

	int x = u.x;
	int y = u.y;
	int z = u.z;

	Vec3i position(x, y, z);
	auto element = new Element(this, type, position);
	for (auto property : properties)
		element->set_property(property);
	element->set_camera_id(camera_id);
	element->set_angle(angle);
	add_element(element);

	while (x != v.x || y != v.y || z != v.z) {
		if (x != v.x)
			x += dx;
		if (y != v.y)
			y += dy;
		if (z != v.z)
			z += dz;

		Vec3i p(x, y, z);
		element = new Element(this, type, p);
		for (auto property : properties)
			element->set_property(property);
		element->set_camera_id(camera_id);
		element->set_angle(angle);
		add_element(element);
	}
}

/**
 * Fill space with elements
 */
void Scene::add_diagonal(Element::Type type, Vec3i u, Vec3i v,
			  int camera_id, int angle)
{
	std::set<Element::Property> properties;
	add_diagonal(type, u, v, camera_id, angle, properties);
}

/**
 * Clear all the content
 */
void Scene::clear()
{
	for (auto entry : elements)
		delete entry.second;

	for (auto guard : guards)
		delete guard;

	elements.clear();
	respawn_spots.clear();
	guards.clear();
	camera.clear();
	lights.clear();
	player = Player(this);

	upper_row = 0;
	lower_row = 0;
	initialized = false;
	status = PLAY;
	exit_style = TOP_ROW;
	coin_score = 0;
	goal_score = 0;
	guards_number = 0;
}

/**
 * Get list of elements
 */
std::vector<Element *> Scene::get_elements() const
{
	std::vector<Element *> result;

	for (auto entry : elements)
		result.push_back(entry.second);

	return result;
}

/**
 * Return main camera location
 */
Vec3 Scene::camera_position() const
{
	return camera.position();
}

/**
 * Return main camera target
 */
Vec3 Scene::camera_target() const
{
	return camera.target();
}

/**
 * Add fixed camera
 * @param position Location of the camera
 * @param target Target of the camera
 */
void Scene::add_camera(int id, const Vec3 &position, const Vec3 &target,
		       double weight)
{
	CameraLocation view;
	view.position = position;
	view.target = target;
	view.weight = weight;
	camera.add_location(id, view);
}

/**
 * Add element in scene.
 */
void Scene::add_element(Element *element)
{
	if (element == nullptr)
		return;

	if (elements.empty()) {
		upper_row = element->grid_y();
		lower_row = element->grid_y();
	} else {
		upper_row = std::max(upper_row, element->grid_y());
		lower_row = std::min(lower_row, element->grid_y());
	}

	auto key = element->grid();
	remove_at(key);
	elements[key] = element;
	initialized = false;
}

/**
 * Add element in scene.
 */
void Scene::add_element(Element::Type type, const Vec3i &position)
{
	auto element = new Element(this, type, position);
	add_element(element);
}

/**
 * Add element in scene.
 */
void Scene::add_element(Element::Type type, int x, int y, int z)
{
	Vec3i position(x, y, z);
	auto element = new Element(this, type, position);
	add_element(element);
}

/**
 * Add object in scene.
 */
void Scene::add_object(Object::Type type, const Vec3i &position)
{
	Element *element = find_element_below(position);

	if (element == nullptr) {
		qWarning() << "Cannot add object here:"
			   << position.x << position.y << position.z;
		return;
	}

	double  height = (double) position.y - element->grid_y();
	element->add_object(type, height);
	initialized = false;
}

/**
 * Find element at absolute position, or the closest one below
 */
Element *Scene::find_element_below(const Vec3i &p) const
{
	Element *found = nullptr;

	for (auto entry : elements) {
		auto key = entry.first;
		if (key.x == p.x && key.z == p.z && key.y <= p.y)
			if (!found || found->grid_y() < key.y)
				found = entry.second;
	}
	return found;
}

/**
 * Initialize dynamic data
 */
void Scene::initialize()
{
	if (initialized)
		return;

	player = Player(this);
	guards.clear();
	respawn_spots.clear();
	goal_score = 0;

	for (auto entry : elements) {
		Element *element = entry.second;
		auto objects = element->get_objects();

		for (auto object : objects) {
			switch (object.type) {
			case Object::COIN:
				goal_score++;
				break;
			case Object::PLAYER: {
				Vec3 position = element->coordinates();
				position.y += object.y - 0.49; // TODO
				player = Player(this, position);
			} break;
			case Object::GUARD: {
				Vec3 position = element->coordinates();
				position.y += object.y - 0.49; // TODO
				Guard *guard = new Guard(this, position);
				guard->initialize(&player);
				guards.push_back(guard);

			} break;
			case Object::RESPAWN: {
				respawn_spots.push_back(element);
			} break;
			default: break;
			}
		}
	}
	guards_number = guards.size();
	initialized = true;
}

const Unit &Scene::get_player() const
{
	return player;
}

std::vector<Guard *> Scene::get_guards() const
{
	std::vector<Guard *> result;
	result.reserve(guards.size());
	for (Guard *guard : guards)
		result.push_back(guard);

	return result;
}

/**
 * Update the scene
 *
 * @param dt Time step
 * @param request Player request
 */
Scene::Status Scene::update(gametime dt, UserCommand command)
{
	if (!initialized)
		initialize();

	for (auto entry : elements)
		entry.second->update(dt);

	respawn_guard();
	std::vector<Unit *> guard_obstacles;

	for (Guard *guard : guards) {
		guard->update(dt, guard_obstacles);
		guard_obstacles.push_back(guard);
	}

	guard_obstacles.clear();
	player.update(dt, command, guard_obstacles);

	camera.select(player.get_base()->get_camera_id());
	camera.set_focus(player.coordinates());
	camera.update(dt);

	check_exit();
	return status;
}

/**
 * Transform user command into in-game player request.
 *
 * Commands are issued by the user (e.g. Going left with keyboard left-arrow
 * key. Requests are in-game intents (going in a X- direction). The location
 * and direction of the camera are used for the conversion. It is also
 * dependent on the elements around the player.
 */
Unit::Request Scene::compute_request(UserCommand command)
{
	if (command == CMD_NONE)
		return Unit::NONE;

	if (command == CMD_STOP)
		return Unit::STOP;

	if (command == CMD_UP && player.check_stop_up())
		return Unit::GO_UP;

	if (command == CMD_DOWN && player.check_stop_down())
		return Unit::GO_DOWN;

	auto request = Unit::NONE;

	const Vec3 view = camera.view_vector();
	double dpx = view.dot(vec3::X1);
	double dpz = view.dot(vec3::Z1);

	if (std::abs(dpx) < std::abs(dpz)) {
		// Looking in Z direction
		Vec3 normal(-view.z, view.y, view.x);
		if (normal.dot(vec3::X1) > 0.0) {
			switch (command) { // Z-
			case CMD_LEFT: request = Unit::GO_XN; break;
			case CMD_DOWN: request = Unit::GO_ZP; break;
			case CMD_UP: request = Unit::GO_ZN; break;
			case CMD_RIGHT: request = Unit::GO_XP; break;
			case CMD_ACTION_LEFT: request = Unit::ACT_XN; break;
			case CMD_ACTION_RIGHT: request = Unit::ACT_XP; break;
			default: break;
			}
		} else { // R2
			switch (command) { // Z+
			case CMD_LEFT: request = Unit::GO_XP; break;
			case CMD_DOWN: request = Unit::GO_ZN; break;
			case CMD_UP: request = Unit::GO_ZP; break;
			case CMD_RIGHT: request = Unit::GO_XN; break;
			case CMD_ACTION_LEFT: request = Unit::ACT_XP; break;
			case CMD_ACTION_RIGHT: request = Unit::ACT_XN; break;
			default: break;
			}
		}
	}
	else {
		// Looking in X direction
		Vec3 normal(view.z, view.y, -view.x);
		if (normal.dot(vec3::Z1) > 0) {
			// R1
			switch (command) { // X-
			case CMD_LEFT: request = Unit::GO_ZP; break;
			case CMD_DOWN: request = Unit::GO_XP; break;
			case CMD_UP: request = Unit::GO_XN; break;
			case CMD_RIGHT: request = Unit::GO_ZN; break;
			case CMD_ACTION_LEFT: request = Unit::ACT_ZP; break;
			case CMD_ACTION_RIGHT: request = Unit::ACT_ZN; break;
			default: break;
			}
		} else {
			// R3
			switch (command) { // X+
			case CMD_LEFT: request = Unit::GO_ZN; break;
			case CMD_DOWN: request = Unit::GO_XN; break;
			case CMD_UP: request = Unit::GO_XP; break;
			case CMD_RIGHT: request = Unit::GO_ZP; break;
			case CMD_ACTION_LEFT: request = Unit::ACT_ZN; break;
			case CMD_ACTION_RIGHT: request = Unit::ACT_ZP; break;
			default: break;
			}
		}
	}
	return request;
}

/**
 * Update lowest and highest Y limits
 */
void Scene::compute_limits()
{
	upper_row = std::numeric_limits<int>::min();
	lower_row = std::numeric_limits<int>::max();

	for (auto entry : elements) {
		int y = entry.second->grid_y();
		if (y > upper_row)
			upper_row = y;
		if (y < lower_row)
			lower_row = y;
	}
}

/**
 * Return element exactly at a given location
 */
Element *Scene::element_at(const Vec3i p) const
{
	auto it = elements.find(p);

	if (it == elements.end())
		return nullptr;

	return it->second;
}

/**
 * Remove an element (and its objects)
 */
void Scene::remove_element(Element *element)
{
	if (element == nullptr) {
		qDebug("Warning: remove null element");
		return;
	}
	initialized = false;

	auto key = element->grid();
	auto it = elements.find(key);

	if (it == elements.end() || element != it->second) {
		qDebug() << "Warning: deleting element unknown to the scene";
		delete element;
		return;
	}

	int y = it->second->grid_y();
	delete it->second;
	elements.erase(it);

	if (y == upper_row || y == lower_row)
		compute_limits();
}

/**
 * Remove element or object at coordinates
 *
 * If the element has objects, they are also deleted.
 *
 * TODO: transfer objects to the element below, if any.
 */
void Scene::remove_at(const Vec3i &p)
{
	Element *element = element_at(p);

	if (element) {
		remove_element(element);
	} else {
		element = find_element_below(p);
		if (element) {
			int height = p.y - element->grid_y();
			element->remove_objects(height, height + 1.0);
		}
	}
}

/**
 * Find obstacle candidate: where to look for depending on player position
 * @param element Reference element
 * @param y Y coordinate of player, relative to reference element
 * @param axis Axis of movement, X or Z
 * @param dir Direction of movement
 * @return relative coordinates of candidate location
 */
Vec3i Scene::find_obstacle_candidate(const Element *element,
				     double y,
				     Axis axis,
				     Direction direction) const
{
	Q_ASSERT(axis == AXIS_X || axis == AXIS_Z);
	Q_ASSERT(direction == DIR_UPW || direction == DIR_DNW);
	bool upwards = (direction == DIR_UPW);

	Vec3i location;

	// Where to look for obstacle in X/Z plane
	if (axis == AXIS_X)
		location.x = upwards ? 1 : -1;
	else
		location.z = upwards ? 1 : -1;

	// Where to look for obstacle, vertically
	double top = element->get_aabb_top();
	double bottom = element->bottom();

	if (!helper::in_range(y, bottom, top))
		qDebug() << "Warning:" << __func__ << "expects y in AABB";

	int cmp = compare(y, (top - bottom) / 2.0 + bottom);
	location.y = cmp > 0 ? 1 : 0;
	return location;
}

/**
 * Find element at a position relative to another one, or if there is none,
 * find the highest lower one with the same X/Z coordinates.
 */
Element *Scene::lower_neighbour(const Element *element, const Vec3i &r) const
{
	Q_ASSERT(element);
	return find_element_below(element->grid() + r);
}

/**
 * Find element at a position relative to another one.
 */
Element *Scene::neighbour(const Element *element, const Vec3i &v) const
{
	Q_ASSERT(element);
	Vec3i p = element->grid() + v;
	return element_at(p);
}

/**
 * Receive event for object retrieval by the player
 */
void Scene::player_takes_object(Object object)
{
	if (object.type == Object::COIN) {
		coin_score++;
		if (coin_score >= goal_score) {
			qDebug() << "Score goal reached";
			unlock();
		}
	} else {
		qDebug() << "Retrieved object of unknown type" << object.type;

	}
}

/**
 * Unlock scene (target goal reached)
 *
 * TODO: replace with signal/slots
 */
void Scene::unlock()
{
	for (auto entry : elements)
		entry.second->unlock();
}

/**
 * Is the level completed?
 */
void Scene::check_exit()
{
	double y = player.coordinates().y;

	if (coin_score >= goal_score && compare(y, upper_row) >= 0)
		status = END_WIN;

	for (Guard *guard : guards)
		if (guard->get_aabb().intersects(player.get_aabb()))
			status = END_FAIL;
}

/**
 * Callback triggered when a player or guard changes its base element
 * @param unit Unit triggering the event
 */
void Scene::on_unit_base_change(Unit *unit)
{
	if (unit != &player)
		return;

	Element *base = unit->get_base();
	Vec3 position = unit->get_rpos();

	camera.select(base->get_camera_id());

	for (Guard *guard : guards)
		guard->set_target(base, position);
}

/**
 * Callback for element expanding back to normal state, while enclosing a unit
 */
void Scene::trap_unit(const Element *closing, const Unit *unit)
{
	Q_ASSERT(closing);

	// Case of guards
	if (unit != nullptr) {
		remove_guard(unit);
		return;
	}

	// Case of player
	if (player.get_aabb().intersects(closing->get_aabb()))
		status = END_FAIL;
}

/**
 * Remove a guard
 */
void Scene::remove_guard(const Unit *guard)
{
	Q_ASSERT(guard);

	auto it = std::find(guards.begin(), guards.end(), guard);

	if (it == guards.end()) {
		qDebug() << "Attempting to delete non-managed guard";
		return;
	}

	delete *it;
	guards.erase(it);
}

/**
 * Attempt to respawn one guard.
 *
 * This function will fail to respawn a guard if the nominal number of guards
 * is already reached, or if the random location to respawn can't be used at
 * the moment (e.g something already there).
 */
void Scene::respawn_guard()
{
	if (respawn_spots.empty())
		return;

	if (guards.size() >= guards_number)
		return;

	int i = rand() % respawn_spots.size();
	Element *spot = respawn_spots[i];

	// TODO don't respawn if a guard is already there

	auto objects = spot->get_objects();

	auto it = std::find_if(objects.begin(), objects.end(),
			       [](const Object &object) {
		return object.type == Object::RESPAWN;
	});

	if (it == objects.end()) {
		qDebug() << "Missing respawn object.";
		return;
	}

	auto position = spot->coordinates();
	position.y += it->y;
	Guard *guard = new Guard(this, position);
	guard->initialize(&player);
	guards.push_back(guard);
}

/**
 * Add a new light to the scene at given coordinates
 */
void Scene::add_light(const Vec3 &position)
{
	lights.push_back(position);
}

/**
 * Get list of light positions
 */
std::vector<Vec3> Scene::get_lights() const
{
	return lights;
}

/**
 * Export static data of the scene to JSON format
 *
 * @param scene_json JSON object to export to
 */
void Scene::write(QJsonObject &scene_json) const
{
	// Elements
	QJsonArray elements_array;
	for (auto entry : elements) {
		QJsonObject json;
		entry.second->write(json);
		elements_array.append(json);
	}
	scene_json["elements"] = elements_array;

	// Lights
	QJsonArray lights_array;
	for (auto light : lights) {
		QJsonObject json;
		light.write(json);
		lights_array.append(json);
	}
	scene_json["lights"] = lights_array;

	// Cameras
	QJsonArray cameras_json;
	camera.write(cameras_json);
	scene_json["cameras"] = cameras_json;
}

/**
 * Import from JSON format
 *
 * @param json JSON object to import
 */
void Scene::read(const QJsonObject &json)
{
	clear();

	// Elements
	if (json.contains("elements") && json["elements"].isArray()) {
		auto array = json["elements"].toArray();
		for (int i = 0; i < array.size(); ++i) {
			Element *element = new Element(this);
			element->read(array[i].toObject());
			add_element(element);
		}
	}

	// Lights
	if (json.contains("lights") && json["lights"].isArray()) {
		auto array = json["lights"].toArray();
		for (int i = 0; i < array.size(); ++i) {
			Vec3 light;
			light.read(array[i].toObject());
			lights.push_back(light);
		}
	}

	// Cameras
	if (json.contains("cameras") && json["cameras"].isArray()) {
		auto array = json["cameras"].toArray();
		camera.read(array);
	}
}
