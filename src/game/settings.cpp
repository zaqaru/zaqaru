/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.hpp"

#include "defs.hpp"

#include <QtGlobal>
#include <QSettings>

#include <sstream>

const char *FULLSCREEN_KEY = "fullscreen";
const char *FILE_KEY = "file";
const char *LEVEL_KEY = "level";
const char *OUTPUT_KEY = "output";

namespace {

QSettings settings(PROJECT_NAME, PROJECT_NAME);

}

/**
 * Immediately save/synchronize settings.
 */
void Settings::save()
{
	settings.sync();
}

/**
 * Check status of last Settings operation and log errors
 *
 * @param func Function handling the operation
 * @param key Key being read/written
 */
void Settings::check_status(const char *func, const char *key)
{
	auto status = settings.status();

	switch (status) {
	case QSettings::NoError: break;
	case QSettings::AccessError:
		qWarning("Settings access error [%s][%s]", func, key);
		break;
	case QSettings::FormatError:
		qWarning("Settings format error [%s][%s]", func, key);
		break;
	default:
		qWarning("Settings unexpected status [%d]", status);
	}
}

/**
 * Retrieve boolean value from local configuration
 */
bool Settings::read_bool(const char *key)
{
	bool value = settings.value(key, false).toBool();
	check_status(__func__, key);
	return value;
}

/**
 * Write boolean value to local configuration
 */
void Settings::write_bool(const char *key, bool value)
{
	settings.setValue(key, value);
	check_status(__func__, key);
}

/**
 * Retrieve string value from local configuration
 */
std::string Settings::read_string(const char *key)
{
	auto value = settings.value(key, "").toString();
	check_status(__func__, key);
	return value.toStdString();
}

/**
 * Write string value to local configuration
 */
void Settings::write_string(const char *key, const std::string &value)
{
	settings.setValue(key, QString(value.c_str()));
	check_status(__func__, key);
}

/**
 * Retrieve int value from local configuration
 */
int Settings::read_integer(const char *key)
{
	auto text = settings.value(key, "").toString();
	check_status(__func__, key);
	int value = 0;

	try {
		value = std::stoi(text.toStdString());
	} catch (const std::exception &e) {
		qWarning("Invalid setting value [%s][%s]", key,
			 text.toStdString().c_str());
	}

	return value;
}

/**
 * Write int value to local configuration
 */
void Settings::write_integer(const char *key, int value)
{
	std::stringstream s;
	s << value;
	settings.setValue(key, QString(s.str().c_str()));
	check_status(__func__, key);
}

/**
 * Retrieve fullscreen mode status from local configuration
 */
bool Settings::read_fullscreen()
{
	return read_bool(FULLSCREEN_KEY);
}

/**
 * Write fullscreen mode status to local configuration
 */
void Settings::write_fullscreen(bool value)
{
	write_bool(FULLSCREEN_KEY, value);
}

/**
 * Retrieve level file name from local configuration
 */
std::string Settings::read_level_file()
{
	return read_string(FILE_KEY);
}

/**
 * Write level file name to local configuration
 */
void Settings::write_level_file(const std::string &value)
{
	write_string(FILE_KEY, value);
}

/**
 * Retrieve level number from local configuration
 */
int Settings::read_level_number()
{
	return read_integer(LEVEL_KEY);
}

/**
 * Write level number to local configuration
 */
void Settings::write_level_number(int value)
{
	write_integer(LEVEL_KEY, value);
}

/**
 * Retrieve output file name from local configuration
 */
std::string Settings::read_output_file()
{
	return read_string(OUTPUT_KEY);
}

/**
 * Write output file name to local configuration
 */
void Settings::write_output_file(const std::string &value)
{
	write_string(OUTPUT_KEY, value);
}
