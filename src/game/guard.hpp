/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GUARD_HPP
#define GUARD_HPP

#include "unit.hpp"

#include "ai.hpp"

class Guard : public Unit
{
public:
	Guard(Scene *const scene, const Vec3 &absolute_position);

	void initialize(const Unit *target);
	void set_target(const Element *, const Vec3 &);
	bool trappable() const override;
	void update(gametime dt, const std::vector<Unit *> &obstacles) override;

	std::vector<Vec3> get_path() const;
	std::vector<Vec3> get_graph() const;

protected:
	bool check_action() override;
	void check_object_presence() override;
	void set_intent(Request = NONE) override;

private:
	int gid;
	PathFinder ai;
};

#endif // GUARD_HPP
