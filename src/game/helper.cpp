/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper.hpp"

#include <cmath>

namespace helper {

const double EPSILON = 0.000001;
const double PI = std::acos(-1.0);

/**
 * Compare 2 double values
 * @return 0 if equal, -1 if first below second parameter, 1 otherwise
 */
int compare(double d1, double d2)
{
	if (std::abs(d1 - d2) < EPSILON)
		return 0;

	return d1 < d2 ? -1 : 1;
}

/**
 * Compare 2 double values for equality
 * @return true if values are considered equal
 */
bool equality(double d1, double d2)
{
	return compare(d1, d2) == 0;
}

/**
 * Compare 2 Vec3 values for equality
 * @return true if vectors are considered equal
 */
bool equality(const Vec3 &u, const Vec3 &v)
{
	return equality(u.x, v.x) && equality(u.y, v.y) && equality(u.z, v.z);
}

/**
 * Indicates wheter a double value is inside a double range
 * @param x Value to check
 * @param x1 Lower limit
 * @param x2 Upper limit
 * @return true if the value is inside the range
 */
bool in_range(double x, double x1, double x2)
{
	return compare(x, x1) >= 0 && compare(x, x2) <= 0;
}

/**
 * Move a double value towards another value, with a maximal step
 * @param value Value to modify (R/W)
 * @param target Final value
 * @param max Maximum step towards final value
 */
void move_closer(double &value, double target, double step)
{
	auto delta = std::abs(step);

	if (equality(value, target)) {
		value = target;
		return;
	}

	if (value > target) {
		value -= delta;
		if (value < target)
			value = target;
	} else {
		value += delta;
		if (value > target)
			value = target;
	}
}

/**
 * Move a double value in radians towards another value, with a maximal step
 * @param value Value to modify (R/W)
 * @param target Final value
 * @param max Maximum step towards final value
 *
 * @pre Value and target should be between -π and π
 */
void move_closer_radians(double &value, double target, double step)
{
	auto delta = std::abs(step);

	if (equality(value, target)) {
		value = target;
		return;
	}

	if (equality(target, -PI) || equality(target, PI)) {
		if (equality(value, -PI) || equality(value, PI)) {
			return;
		}
	}

	// Since this is modulo 2π, closer may mean the opposite direction
	if (std::abs(target - value) > PI) {
		if (value > target) {
			value += delta;
			if (value > PI)
				value -= 2 * PI;
		} else {
			value -= delta;
			if (value < 0)
				value += 2 * PI;
		}
		// return; // works better without
	}

	if (value > target) {
		value -= delta;
		if (value < target)
			value = target;
	} else {
		value += delta;
		if (value > target)
			value = target;
	}
}

/**
 * Move a Vector3 towards another one, with a maximal step on each axis
 * @param v Vector to modify
 * @param target Target value
 * @param delta Step
 */
void move_closer(Vec3 &v, const Vec3 &target, double delta)
{
	move_closer(v.x, target.x, delta);
	move_closer(v.y, target.y, delta);
	move_closer(v.z, target.z, delta);
}

/**
 * Get direction as an enum rather than real
 * @param x Direction quantity (zero, positive, or negative)
 * @return Same direction expressed as direction enum
 */
Direction get_direction(double x)
{
	int cmp = compare(x, 0.0);
	return (cmp == 0) ? DIR_NONE : (cmp > 0) ? DIR_UPW : DIR_DNW;
}

/**
 * Whether a string contains exactly another one at its end.
 * @return
 */
bool string_ends_with(const std::string &source, const std::string &pattern)
{
	auto index = source.rfind(pattern);
	return index == source.size() - pattern.size();
}

} // helper
