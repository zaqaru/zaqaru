/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "element.hpp"

#include "helper.hpp"
#include "scene.hpp"
#include "unit.hpp"

#include <QDebug>
#include <QJsonArray>

using helper::compare;
using helper::in_range;

// Element stays destroyed only for these successive durations
const unsigned int SHRINK_TIME = 500;
const unsigned int DESTROYED_TIME = 5000;
const unsigned int EXPAND_TIME = 1000;

// Duration for traps appearing as revealed
const unsigned int REAPPEAR_TIME  = 3000;

const std::set<Element::Property> SERIALIZABLE_PROPERTIES = {
	Element::APPEAR_ON_UNLOCK,
	Element::DISAPPEAR_ON_UNLOCK
};

/**
 * Constructor
 */
Element::Element(Scene *root)
	: element_type(NONE), scene(root)
{
}

/**
 * Constructor
 * @param t Type of element
 * @param p Position
 */
Element::Element(Scene *root, Type t, const Vec3i &p)
	: position(p), element_type(t), scene(root)
{
	setup();
}

/**
 * Destructor
 */
Element::~Element()
{
}

/**
 * Setup element
 *
 * Set various attributes according to element type (field, properties).
 */
void Element::setup()
{
	switch (element_type) {
	case BLOCK:
		set_property(DESTROYABLE);
		set_property(OBSTRUCTING);
		set_property(OBSTRUCTING_BOTTOM);
		set_property(OBSTRUCTING_TOP);
		set_property(WALKABLE_TOP);
		break;
	case TOUGH:
	case USLOPE:
		set_property(OBSTRUCTING);
		set_property(OBSTRUCTING_BOTTOM);
		set_property(OBSTRUCTING_TOP);
		set_property(WALKABLE_TOP);
		break;
	case VFIELD:
		set_property(CLIMBABLE);
		set_property(CROSSABLE);
		set_property(CROSSABLE_BOTTOM);
		set_property(CROSSABLE_TOP);
		set_property(WALKABLE_BOTTOM);
		set_property(WALKABLE_TOP);
		break;
	case HFIELD:
		set_property(CROSSABLE);
		set_property(CROSSABLE_BOTTOM);
		set_property(WALKABLE_BOTTOM);
		break;
	case TRAP:
		set_property(OBSTRUCTING);
		set_property(OBSTRUCTING_BOTTOM);
		set_property(CROSSABLE);
		set_property(CROSSABLE_BOTTOM);
		break;
	case USTREAM:
		set_property(CLIMBABLE);
		set_property(CROSSABLE_TOP);
		set_property(OBSTRUCTING_TOP);
		set_property(WALKABLE_TOP);
		field = VERTICAL_FIELD;
		break;
	case HSTREAM:
		set_property(CROSSABLE);
		set_property(WALKABLE_BOTTOM);
		field = HORIZONTAL_FIELD;
		break;
	case SLOPE:
		set_property(OBSTRUCTING);
		set_property(OBSTRUCTING_BOTTOM);
		set_property(OBSTRUCTING_TOP);
		set_property(SLIDING);
		set_property(WALKABLE_TOP);
		break;
	default:
		qDebug() << "Missing properties for type" << element_type;
	}
}

void Element::clear()
{
	objects.clear();
	properties.clear();
	position.clear();
	angle = 0;
	element_type = NONE;
	state = NORMAL;
	field = NO_FIELD;
	active = true;
	camera_id = 0;
	time = 0;
	blocker = nullptr;
}

/**
 * Return the element position
 */
Vec3i Element::grid() const
{
	return position;
}

/**
 * Return X grid coordinate
 */
int Element::grid_x() const
{
	return position.x;
}

/**
 * Return Y grid coordinate
 */
int Element::grid_y() const
{
	return position.y;
}

/**
 * Return Z grid coordinate
 */
int Element::grid_z() const
{
	return position.z;
}

/**
 * Return coordinates as Vec3
 */
Vec3 Element::coordinates() const
{
	return Vec3(position);
}

/**
 * Return the element type
 */
Element::Type Element::type() const
{
	return element_type;
}

/**
 * Return relative height of the bottom of the element.
 */
double Element::bottom() const
{
	return -0.5;
}

/**
 * Return relative height of the top of the element's AABB.
 */
double Element::get_aabb_top() const
{
	return 0.5;
}

/**
 * Set boolean property (see values in class definition)
 */
void Element::set_property(Property property)
{
	properties.insert(property);

	if (property == APPEAR_ON_UNLOCK)
		active = false;
}

/**
 * Add an object to the element.
 *
 * Objects are centered on their element, and placed at some height above.
 */
void Element::add_object(Object::Type type, double y)
{
	double top = get_aabb_top();

	if (y < top)
		qDebug() << "Object below top" << y << top;

	Object object { type, y };
	objects.push_back(object);
}

/**
 * Get the list of objects on this element
 */
std::vector<Object> Element::get_objects() const
{
	return objects;
}

/**
 * Indicate wheteher a unit can walk/stand on top of the element
 *
 * This is independent of the sliding property
 */
bool Element::is_walkable_top(const Unit *unit) const
{
	if (!active)
		return false;

	if (!has_property(WALKABLE_TOP))
		return false;

	if (is_destroyed())
		return is_blocked(unit);

	return true;
}

/**
 * Indicate wheteher a unit can walk/stand on top of the element
 *
 * This is used by AI, ignoring destroyed state
 */
bool Element::is_walkable_top() const
{
	if (!active)
		return false;

	if (!has_property(WALKABLE_TOP))
		return false;

	return true;
}

/**
 * Indicate wheteher a unit can move on top of the element
 *
 * This is for use by AI graph, when direction matters
 */
bool Element::is_walkable_top(Axis xz, Direction dir) const
{
	if (!active)
		return false;

	if (!has_property(WALKABLE_TOP))
		return false;

	if (has_property(SLIDING)) {
		Vec3 v = create_vec3(xz, dir).normalized();
		Vec3 ep = get_exit_point();
		ep.y = 0.0;
		return helper::equality(v, ep.normalized());
	}

	return true;
}

/**
 * Would the player fall when inside the element?
 */
bool Element::is_climbable() const
{
	if (!active)
		return false;

	return has_property(CLIMBABLE);
}

/**
 * Whether the given unit would walk/stand at the bottom of the element
 */
bool Element::is_walkable_bottom(const Unit *unit) const
{
	if (!active)
		return false;

	if (!has_property(WALKABLE_BOTTOM))
		return false;

	if (unit == nullptr || unit->trappable())
		return !is_destroyed();

	return true;
}

/**
 * Whether a unit would walk/stand at the bottom of the element
 *
 * For use by AI (destruction state ignored)
 */
bool Element::is_walkable_bottom() const
{
	if (!active)
		return false;

	if (has_property(WALKABLE_BOTTOM))
		return true;

	return false;
}

/**
 * Whether the element is in a destroyed state
 */
bool Element::is_destroyed() const
{
	return state == SHRINK || state == DESTROYED || state == EXPAND;
}

/**
 * Whether the element is blocked for a given unit (guard/player).
 */
bool Element::is_blocked(const Unit *unit) const
{
	return blocker != nullptr && blocker != unit;
}

/**
 * Whether the element is obstructing units (from the sides)
 */
bool Element::is_obstructing(bool dynamic) const
{
	if (!active)
		return false;

	if (!has_property(OBSTRUCTING))
		return false;

	if (dynamic && has_property(DESTROYABLE))
		return !is_destroyed();

	return true;
}

/**
 * Whether the element is obstructing units (from under the element)
 */
bool Element::is_obstructing_bottom(bool dynamic) const
{
	if (!active)
		return false;

	if (!has_property(OBSTRUCTING_BOTTOM))
		return false;

	if (dynamic && has_property(DESTROYABLE) && is_destroyed())
		return false;

	return true;
}

/**
 * Whether the element is obstructing units (from above the element)
 */
bool Element::is_obstructing_top(bool dynamic) const
{
	if (!active)
		return false;

	if (!has_property(OBSTRUCTING_TOP))
		return false;

	if (dynamic && has_property(DESTROYABLE) and is_destroyed())
		return false;

	return true;
}

/**
 * Whether a relative postion is inside the element
 */
bool Element::is_inside(const Vec3 &p) const
{
	return (compare(p.x, -0.5) >= 0 &&
		compare(p.x, 0.5) <= 0 &&
		compare(p.y, bottom()) >= 0 &&
		compare(p.y, get_top(p.x, p.z)) <= 0 &&
		compare(p.z, -0.5) >= 0 &&
		compare(p.z, 0.5) <= 0);
}

/**
 * Check presence of a boolean property
 */
bool Element::has_property(Property property) const
{
	return properties.find(property) != properties.end();
}

/**
 * Remove objects in specific height interval
 *
 * This is only used for edition: no gameplay event is triggered.
 */
void Element::remove_objects(double y1, double y2)
{
	auto it = objects.begin();
	while (it != objects.end()) {
		if (helper::in_range(it->y, y1, y2))
			it = objects.erase(it);
		else
			++it;
	}
}

/**
 * Is the element allowing the unit to exit laterally?
 *
 * In practice this is only relevant for trapped guards.
 */
bool Element::is_crossable_inside() const
{
	return !active || !is_blocked(nullptr);
}


/**
 * Can the player decide to go down from inside the element?
 */
bool Element::is_crossable_bottom() const
{
	if (!active)
		return true;

	return has_property(CROSSABLE_BOTTOM);
}

/**
 * Can the player decide to go up from inside the element?
 */
bool Element::is_crossable_top() const
{
	if (!active)
		return true;

	return has_property(CROSSABLE_TOP);
}

/**
 * Can the player decide to go out laterally from inside the element?
 */
bool Element::is_crossable(Axis xz, Direction dir) const
{
	if (!active)
		return true;

	if (!has_property(CROSSABLE))
		return false;

	if (!has_field())
		return true;

	if (field != HORIZONTAL_FIELD)
		return false;

	Vec3 v = get_field_velocity().normalized();
	Vec3 expected = create_vec3(xz, dir);

	return helper::equality(expected, v);
}

/**
 * Is element currently active
 */
bool Element::is_active() const
{
	return active;
}

/**
 * Start element destruction phase if possible.
 *
 * @return true if this request actually started a destruction.
 */
bool Element::destroy()
{
	if (!active)
		qDebug() << "Trying to destroy inactive element";

	if (is_destroyable()) {
		set_state(SHRINK, SHRINK_TIME);
		return true;
	}

	return false;
}

/**
 * Indicates whether the element may be destroyed now.
 *
 * @return true if the element may be destroyed
 */
bool Element::is_destroyable() const
{
	if (!active || !has_property(DESTROYABLE))
		return false;

	// If there's an object on the ground, can't be destroyed
	for (auto &object : objects)
		if (object.is_game_object())
			if (compare(object.y, get_aabb_top() + 1.0) < 0)
				return false;

	// If already destroyed or in transition, it can't be destroyed
	if (state != NORMAL)
		return false;

	return true;
}

/**
 * Update element
 * @param dt Time step
 */
void Element::update(gametime dt)
{
	if (state == NORMAL)
		return;

	time = dt < time ? time - dt : 0;

	if (time > 0)
		return;

	switch (state) {
	case SHRINK:
		set_state(DESTROYED, DESTROYED_TIME);
		break;
	case DESTROYED:
		set_state(EXPAND, EXPAND_TIME);
		break;
	case EXPAND:
		set_state(NORMAL, 0);
		scene->trap_unit(this, blocker);
		unblock();
		break;
	case REVEALED:
		set_state(NORMAL, 0);
		active = true;
		break;
	case NORMAL:
		break;
	default:
		qDebug() << "Warning: Element update with unhandled state";
	}
}

/**
 * Block an element (prevent others from getting in)
 * @param unit Unit responsible for blocking the element
 *
 * This is done by a unit (a guard falling into a destroyed element):
 * the element is then blocked for the other units (guards, player).
 */
void Element::block(const Unit *unit)
{
	if (blocker != nullptr)
		qDebug() << "Blocking already-blocked element";

	blocker = unit;
}

/**
 * Unblock the element if it was previously blocked by a unit.
 */
void Element::unblock()
{
	blocker = nullptr;
}

/**
 * Callback for unit movement inside element
 */
void Element::on_move_inside()
{
	if (element_type != TRAP)
		return;

	set_state(REVEALED, REAPPEAR_TIME);
	active = false;
}

/**
 * Remove and return object at specific location if it exists
 * @return removed object or NONE if inexistant
 */
Object Element::give_object_at(const Box &source)
{
	for (auto object = objects.begin(); object != objects.end(); ++object) {
		if (!object->is_game_object())
			continue;

		// We check the collision with hardcoded values
		Vec3 p(this->coordinates());
		p.y += object->y;
		Vec3 p1(p + Vec3(-0.2, -0.2, -0.2));
		Vec3 p2(p + Vec3(0.2, 0.2, 0.2));
		Box target(p1, p2);

		if (source.intersects(target)) {
			Object copy = *object;
			objects.erase(object);
			return copy;
		}
	}
	return { Object::NONE, 0 };
}

/**
 * Unlock element
 */
void Element::unlock()
{
	if (has_property(APPEAR_ON_UNLOCK))
		active = true;

	if (has_property(DISAPPEAR_ON_UNLOCK))
		active = false;
}

/**
 * Associate element with a camera
 * @param id
 */
void Element::set_camera_id(int id)
{
	camera_id = id;
}

/**
 * Get id of camera associated with this element
 */
int Element::get_camera_id() const
{
	return camera_id;
}

/**
 * Get bounding box
 */
Box Element::get_aabb() const
{
	Vec3 lower(Vec3(position) + Vec3(-0.5, bottom(), -0.5));
	Vec3 upper(Vec3(position) + Vec3(0.5, get_aabb_top(), 0.5));
	return Box(lower, upper);
}

/**
 * Whether the elements has a field (USTREAM, DSTREAM, HSTREAM)
 */
bool Element::has_field() const
{
	return field != NO_FIELD;
}

/**
 * Get velocity vector of element field. Null vector is returned if there
 * is no field associated with the element.
 */
Vec3 Element::get_field_velocity() const
{
	switch (field) {
	case NO_FIELD:
		return Vec3();
	case VERTICAL_FIELD:
		return Vec3(0.0, 0.01, 0.0);
	case HORIZONTAL_FIELD: {
		Vec3 v(0.01, 0.0, 0.0);
		return v.rotated(angle);
	}
	default:
		qDebug() << "Unhandled field type";
	}
	return Vec3();
}

/**
 * Set element orientation around Y axis.
 * @param a angle (any of 0, 90, 180, 270)
 */
void Element::set_angle(unsigned int a)
{
	angle = a % 360;
	if (a % 90 != 0) {
		qDebug() << __FILE__ ":" << __LINE__ << ":Invalid angle";
		angle = 0;
	}
}

/**
 * Get rotation angle around Y axis
 */
int Element::get_angle() const
{
	return angle;
}

/**
 * Return relative height of the top of the element.
 */
double Element::get_top(double rx, double rz) const
{
	Vec3 p(rx, 0.5, rz);

	if (angle != 0)
		p = p.rotated(360 - angle);

	if (element_type == SLOPE)
		p.y = - p.z;

	return p.y;
}

/**
 * Whether the element is a slider (slope)
 */
bool Element::is_slider() const
{
	return has_property(SLIDING);
}

/**
 * Get exit point (in case of sliding)
 */
Vec3 Element::get_exit_point() const
{
	switch (element_type) {
	case SLOPE: {
		Vec3 v(0.0, -0.5, 0.5);
		return v.rotated(angle);
	}
	default:
		qDebug() << "Unexpected call to" << __func__;
	}
	return Vec3(0.0, 0.5, 0.5);
}

/**
 * Export element to JSON format
 *
 * @param element_json JSON object to export to
 */
void Element::write(QJsonObject &element_json) const
{
	// Element type
	element_json["type"] = element_type;

	// Position
	QJsonObject position_json;
	position.write(position_json);
	element_json["position"] = position_json;

	// Camera ID, if non-zero
	if (camera_id != 0)
		element_json["camera"] = camera_id;

	// Angle is stored only if non-zero
	if (angle != 0)
		element_json["angle"] = angle;

	// Objects of the element, as an optional array
	QJsonArray objects_array;
	for (auto object : objects) {
		QJsonObject json;
		json["type"] = object.type;
		json["y"] = object.y;
		objects_array.append(json);
	}
	if (!objects.empty())
		element_json["objects"] = objects_array;

	// Properties: only a limited number of properties are serialized.
	// The others are defined according to the element types
	QJsonArray properties_array;
	for (auto property : properties) {
		if (SERIALIZABLE_PROPERTIES.count(property) > 0)
			properties_array.append(property);

	}
	if (!properties_array.empty())
		element_json["properties"] = properties_array;
}

/**
 * Import from JSON format
 *
 * @param json JSON object to import
 */
void Element::read(const QJsonObject &json)
{
	clear();

	element_type = Type(json["type"].toInt());
	position.read(json["position"].toObject());
	camera_id = json["camera"].toInt();
	angle = json["angle"].toInt();

	if (json.contains("objects") && json["objects"].isArray()) {
		auto array = json["objects"].toArray();
		for (int i = 0; i < array.size(); ++i) {
			QJsonObject json_object = array[i].toObject();
			auto type = Object::Type(json_object["type"].toInt());
			auto y = json_object["y"].toDouble();
			add_object(type, y);
		}
	}

	if (json.contains("properties") && json["properties"].isArray()) {
		auto array = json["properties"].toArray();
		for (int i = 0; i < array.size(); ++i) {
			Property p = Property(array[i].toInt());
			set_property(p);
		}
	}

	setup();
}

/**
 * Set new element state as well as its internal time
 */
void Element::set_state(State new_state, gametime new_time)
{
	state = new_state;
	time = new_time;
}
