/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "level.hpp"

#include "scene.hpp"

#include <QDebug>

#include <algorithm>
#include <sstream>

const int DAT_BUFFER_SIZE = 512;
const int SCL_BUFFER_SIZE = 180;
const int RESPAWN_ROW = 14; // for both DAT and SCL

const std::set<Level::Tile> Level::STRUCTURAL_ELEMENTS = {
	EMPTY, BRICK, CEMENT, LADDER, RAIL, FAKE, ESCAPE, HIDDEN, RAILX,
};

/**
 * Constructor
 * @param file Path/name of the level file
 * @param number Level number
 */
Level::Level(const std::string &file, int number)
	: filename(file), initialized(false), level_number(number)
{
}

/**
 * Destructor
 */
Level::~Level()
{
	if (input.is_open())
		input.close();
}


/**
 * Read unsigned long from input stream.
 * @throws runtime_error
 */
unsigned long Level::read_unsigned_long(std::ifstream &in)
{
	char c = 0;
	unsigned long value = 0;

	for (int i = 3; i >= 0; --i) {
		in.get(c);
		if (!in.good())
			throw std::runtime_error("Could not read file.");

		value |= (c & 0xFF) << (i * 8);
	}
	return value;
}

/**
 * Initialize level file.
 * Check actual file, get type and number of levels.
 */
void Level::initialize()
{
	if (initialized)
		return;

	// Find type of level
	level_type = UNKNOWN;
	if (filename.size() >= 4) {
		auto extension = filename.substr(filename.size() - 4, 4);
		if (extension == ".scl")
			level_type = SCL;
		else if (extension == ".dat")
			level_type = DAT;
	}
	if (level_type == UNKNOWN)
		throw std::runtime_error("Unknown type of level file.");

	// Check/open input stream
	if (input.is_open())
		input.seekg(0, std::ios::beg);
	else
		input.open(filename.c_str(), std::ios::binary);

	if (!input.good())
		throw std::runtime_error("Failed to open level file.");

	// Specific initialization
	switch (level_type) {
	case SCL:
		init_scl();
		break;
	case DAT:
		init_dat();
		break;
	default:
		qWarning() << "Unexpected level type.";
		return;
	}
	initialized = true;
}

/**
 * Initialize SCL level
 * @throws runtime_error Read failure or incorrect file
 */
void Level::init_scl()
{
	std::string id;
	char c = 0;

	for (int i = 0; i < 4; ++i) {
		input.get(c);
		if (!input.good())
			throw std::runtime_error("Failed to read identifier.");

		id.push_back(c);
	}
	if (id != "SCAV")
		throw std::runtime_error("Invalid identifier in SCL file.");

	auto total = read_unsigned_long(input);
	total_levels = total / 2;
}

/**
 * Initialize DAT level
 * @throws runtime_error Read failure or incorrect file
 */
void Level::init_dat()
{
	input.seekg(0, std::ios::end);
	long length = input.tellg();

	if (length % DAT_BUFFER_SIZE != 0)
		throw std::runtime_error("Invalid length of DAT file.");

	total_levels = std::min<int>(length / DAT_BUFFER_SIZE, 150);
}

/**
 * Return the number of levels in the level file
 */
int Level::size() const
{
	return total_levels;
}

/**
 * Load level. Data is imported in provided scene.
 * If level number is invalid, return without loading anything.
 * @throws std::runtime_error in case of failure
 */
void Level::load(Scene &scene, int level)
{
	if (level < 1 || level > total_levels) {
		qWarning("Requested invalid level number %d.", level);
		return;
	}

	qDebug("Loading level %d", level);
	level_name.clear();
	unknown_ids.clear();
	cells.clear();
	xmin = std::numeric_limits<int>::max();
	xmax = std::numeric_limits<int>::min();
	ymin = std::numeric_limits<int>::max();
	ymax = std::numeric_limits<int>::min();

	switch (level_type) {
	case SCL: load_scl(level); break;
	case DAT: load_dat(level); break;
	default:
		qWarning("Invalid level type.");

	}
	build_scene(scene);

	// Camera
	double width = 1.0 + xmax - xmin;
	double height = 1.0 + ymax - ymin;
	double cx = xmin + width / 2.0;
	double cy = ymin + height / 2.0;
	double cz = width * 0.73; // empirical
	scene.add_camera(0, { cx, cy, cz }, { cx, cy, 0.0 });

	// Lights
	scene.add_light(Vec3(5.0, 10.0, 3.0));
	scene.add_light(Vec3(20.0, 5.0, 3.0));

	// Warning about unknown ids found in the level
	if (unknown_ids.size() > 0) {
		std::stringstream message;
		message << "Unknown elements found in level:";
		for (auto &n : unknown_ids)
			message << " [" << n << "]";

		qWarning(message.str().c_str());
	}
}

/**
 * Load SCL level. Data is imported in provided scene.
 * @throws std::runtime_error in case of failure
 */
void Level::load_scl(int level)
{
	// Get level offset and length
	input.seekg(16 * level - 8, std::ios::beg);
	if (!input.good())
		throw std::runtime_error("Could not access index.");

	auto offset = read_unsigned_long(input);
	auto length = read_unsigned_long(input);

	if (offset == 0 || length != SCL_BUFFER_SIZE)
		throw std::runtime_error("Invalid index.");

	// Level name
	level_name = "Scavenger";

	// Read level
	input.seekg(offset, std::ios::beg);
	if (!input.good())
		throw std::runtime_error("Could not access level.");

	char buffer[SCL_BUFFER_SIZE];
	input.read(buffer, length);
	if (!input.good())
		throw std::runtime_error("Could not read level.");

	// Extract data. In the source buffer, the level is stored line by
	// line (from top to bottom), with 2 elements per byte (4 bits each).
	// There are 15 lines and 24 columns.
	char *p = buffer;
	for (int y = 0; y < 15; ++y) {
		for (int x = 0; x < 12; ++x) {
			store_element(Tile((*p >> 4) & 0x0F), 2 * x, 14 - y);
			store_element(Tile(*p & 0x0F), 2 * x + 1, 14 - y);
			++p;
		}
	}
}

/**
 * Load DAT level. Data is imported in provided scene.
 * @throws std::runtime_error in case of failure
 */
void Level::load_dat(int level)
{
	input.seekg((level - 1) * DAT_BUFFER_SIZE, std::ios::beg);
	if (!input.good())
		throw std::runtime_error("Could not access level.");

	char buffer[DAT_BUFFER_SIZE];
	input.read(buffer, DAT_BUFFER_SIZE);
	if (!input.good())
		throw std::runtime_error("Could not read level.");

	// Remove obfuscation
	for (int n = 0; n < DAT_BUFFER_SIZE; ++n) {
		unsigned long m = (0xE3B2E3B2 >> ((n % 32) / 2)) & 0xFFFF;
		buffer[n] ^= (n & 1 ? m & 0xFF : m >> 8);
	}

	// Level name
	level_name = std::string(&buffer[0x1F5], 11);

	// Extract data
	for (int x = 0; x < 26; ++x) {
		for (int y = 0; y < 16; ++y) {
			store_element(Tile(buffer[x * 16 + y]), x, 15 - y);
		}
	}
}

/**
 * Store an element found in the level.
 *
 * The scene will be built only once all these elements have been stored.
 *
 * @param x X coordinate of new element
 * @param y Y coordinate of new element
 */
void Level::store_element(Tile tile, int x, int y)
{
	cells[Vec2i(x, y)] = tile;

	xmin = std::min(xmin, x);
	xmax = std::max(xmax, x);
	ymin = std::min(ymin, y);
	ymax = std::max(ymax, y);
}

/**
 * Add stored elements to scene.
 *
 * Structural elements are added first, and then objects and player positions.
 */
void Level::build_scene(Scene &scene)
{
	fix_rails();
	add_respawn_spots();
	add_structure(scene);
	add_objects(scene);
}

/**
 * Add structural elements to scene.
 */
void Level::add_structure(Scene &scene)
{
	bool access_bottom = false;
	bool access_top = false;

	// Add stored elements
	for (auto it : cells) {
		auto tile = it.second;

		if (STRUCTURAL_ELEMENTS.count(tile) == 0)
			continue;

		Element::Type type = Element::NONE;
		bool appear_on_unlock = false;
		bool disappear_on_unlock = false;
		Vec2i position = it.first;

		switch (tile) {
		case EMPTY:
			type = Element::NONE;
			break;
		case BRICK:
		case HIDDEN:
			type = Element::BLOCK;
			break;
		case CEMENT:
			type = Element::TOUGH;
			break;
		case LADDER:
			type = Element::VFIELD;
			break;
		case RAIL:
			type = Element::HFIELD;
			break;
		case FAKE:
			type = Element::TRAP;
			break;
		case ESCAPE:
			type = Element::VFIELD;
			appear_on_unlock = true;
			break;
		case RAILX:
			type = Element::HFIELD;
			disappear_on_unlock = true;
			break;
		default:
			unknown_ids.insert(tile);
			break;
		}

		if (type != Element::NONE) {
			Element *element = new Element(&scene, type, Vec3i(position));
			if (appear_on_unlock)
				element->set_property(Element::APPEAR_ON_UNLOCK);
			if (disappear_on_unlock)
				element->set_property(Element::DISAPPEAR_ON_UNLOCK);
			scene.add_element(element);
		}

		if (position.y == ymin && tile != CEMENT)
			access_bottom = true;

		if (position.y == ymax && tile == LADDER)
			access_top = true;
	}

	// Fix level to prevent access to top/bottom
	fill_additional_rows(scene, access_top, access_bottom);
}

/**
 * Add objects and player positions to scene.
 */
void Level::add_objects(Scene &scene)
{
	for (auto it : cells) {
		auto tile = it.second;

		if (STRUCTURAL_ELEMENTS.count(tile) > 0)
			continue;

		Vec2i position(it.first);

		switch (tile) {
		case ENEMY:
			scene.add_object(Object::GUARD, Vec3i(position));
			break;
		case HERO:
			scene.add_object(Object::PLAYER, Vec3i(position));
			break;
		case GOLD:
		case FLASHING:
			scene.add_object(Object::COIN, Vec3i(position));
			break;
		case RESPAWN:
			scene.add_object(Object::RESPAWN, Vec3i(position));
			break;
		default:
			unknown_ids.insert(tile);
		}
	}
}

/*
 * Get name of the level
 */
std::string Level::name() const
{
	return level_name;
}

/**
 * Fill additional rows. 2D levels may need additional rows to be
 * playable (bottom row) or to avoid unwanted shortcuts (top row).
 * @param scene Scene
 * @param top Should fill top row
 * @param bottom Should fill bottom row
 */
void Level::fill_additional_rows(Scene &scene, bool top, bool bottom)
{
	if (top) {
		for (auto it : cells) {
			int x = it.first.x;
			int y = it.first.y;
			Tile id = it.second;

			if (y == ymax) {
				Element *element = nullptr;
				Element::Type type = Element::BLOCK;
				Vec3i position(x, y + 1, 0);

				switch (id) {
				case LADDER:
					type = Element::VFIELD;
					element = new Element(&scene, type, position);
					element->set_property(Element::APPEAR_ON_UNLOCK);
					break;
				default:
					element = new Element(&scene, type, position);
					break;
				}
				Q_ASSERT(element != nullptr);
				scene.add_element(element);
			}
		}
	}

	if (bottom) {
		for (int x = xmin; x <= xmax; ++x) {
			Vec2i position(x, ymin - 1);
			auto element = new Element(&scene, Element::TOUGH, Vec3i(position));
			scene.add_element(element);
		}
	}
}

/**
 * Fix some awkward cases of rails above other elements. This is because our
 * HFIELD element is visually different than rails from classic games.
 */
void Level::fix_rails()
{
	for (auto it : cells) {
		Vec2i p = it.first;
		Tile tile = it.second;

		if (tile != RAIL)
			continue;

		auto under = cells.find(p + Vec2i(0, -1));
		if (under == cells.end())
			continue;

		Vec2i q = under->first;
		switch (under->second) {
		case BRICK:
			cells[p] = EMPTY;
			cells[q] = CEMENT;
			break;
		case CEMENT:
		case LADDER:
			cells[p] = EMPTY;
			break;
		case FAKE:
			// Not strictly equivalent but seems
			// to be fine in most levels
			cells[q] = EMPTY;
			break;
		case ESCAPE:
			cells[p] = RAILX;
			break;
		default: break; // Nothing
		}
	}
}

/**
 * Define respawn spots, at preferred height, otherwise highest possible row.
 */
void Level::add_respawn_spots()
{
	// Possible respawn heights, by preferred order
	std::vector<int> heights;
	heights.push_back(RESPAWN_ROW);
	for (int i = ymax; i >= ymin; i--)
		heights.push_back(i);

	for (int height : heights) {
		bool line_used = false;
		for (auto it : cells) {
			if (it.second == EMPTY && it.first.y == height) {
				cells[it.first] = RESPAWN;
				line_used = true;
			}
		}
		if (line_used)
			break;
	}
}
