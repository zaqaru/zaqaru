/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "defs.hpp"
#include "vec.hpp"

#include <QJsonObject>

#include <map>

/**
 * Location of the camera. This is one of the places where the camera may
 * move to, depending on external events, like the player location.
 */
struct CameraLocation
{
	Vec3 position;
	Vec3 target;
	double weight;
};

/**
 * Current camera and its locations
 */
class Camera
{
public:
	Camera();
	void add_location(int id, CameraLocation);
	void clear();
	void select(int);
	void set_focus(const Vec3 &vec);
	void update(gametime dt);

	Vec3 position() const;
	Vec3 target() const;
	Vec3 view_vector() const;

	// Serialization
	void read(const QJsonArray &array);
	void write(QJsonArray &array) const;

private:
	void update_location(Vec3 &location, Vec3 &speed,
			     const Vec3 &, gametime dt);

	CameraLocation current;
	Vec3 position_speed;
	Vec3 target_speed;
	Vec3 focus;
	int preferred = 0;

	std::map<int, CameraLocation> locations;
};

#endif // CAMERA_HPP
