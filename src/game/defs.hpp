/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFS_HH
#define DEFS_HH

#include <cstdint>

#define PROJECT_NAME "Zaqaru"
#define PROJECT_VERSION "0.2"
#define VERSION_STATUS "dev"

typedef uint32_t gametime;

enum UserCommand {
	CMD_NONE,
	CMD_EXIT_PROGRAM,
	CMD_NEXT_LEVEL,
	CMD_RESTART_LEVEL,
	CMD_FULLSCREEN,
	CMD_DOWN,
	CMD_LEFT,
	CMD_RIGHT,
	CMD_UP,
	CMD_STOP,
	CMD_ACTION_LEFT,
	CMD_ACTION_RIGHT,
	CMD_SAVE,
};

enum Axis {
	AXIS_X, AXIS_Y, AXIS_Z
};

enum Direction {
	DIR_NONE, DIR_UPW, DIR_DNW
};

#endif
