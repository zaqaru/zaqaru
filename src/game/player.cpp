/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "player.hpp"

#include "helper.hpp"
#include "scene.hpp"

#include <QDebug>

using helper::compare;

/**
 * Constructor
 */
Player::Player(Scene *const root, const Vec3 &absolute_position)
	: Unit(root, absolute_position)
{
	initialize();
}

/**
 * Common initialization for all constructors
 */
void Player::initialize()
{
	walk_speed = 0.0045;
	climb_speed = 0.0045;
	fall_speed = 0.0055;
}

/**
 * Whether the player may be trapped by a destroyed block (always false)
 */
bool Player::trappable() const
{
	return false;
}

/**
 * Update player intent
 */
void Player::update(gametime dt, UserCommand command,
		    const std::vector<Unit *> &obstacles)
{
	auto request = scene->compute_request(command);

	if (request != Request::NONE)
		intent = request;

	Unit::update(dt, obstacles);
}


/**
 * Check action request
 * @param dt Time step
 */
bool Player::check_action()
{
	Vec3i next;

	switch (intent) {
	case Request::ACT_XN: next.x = -1; break;
	case Request::ACT_XP: next.x = 1; break;
	case Request::ACT_ZN: next.z = -1; break;
	case Request::ACT_ZP: next.z = 1; break;
	default: return false;
	}

	intent = Request::STOP;

	// Identify element to destroy
	next.y = compare(position.y, 0.0) >= 0 ? 0 : -1;

	// Can't destroy if there's an obstacle
	const Element *obstacle = scene->neighbour(base, next + vec3i::Y1);
	if (obstacle && obstacle->is_active() && !obstacle->is_destroyed())
		return true;

	// Destroy element if possible
	Element *target = scene->neighbour(base, next);
	if (target) {
		bool succeed = target->destroy();
		if (!succeed)
			qDebug() << "Tried to destroy, but failed.";
	}
	return true;
}

/**
 * Check object presence
 */
void Player::check_object_presence()
{
	Q_ASSERT(base);

	auto object = base->give_object_at(get_aabb());

	if (object.type != Object::NONE)
		scene->player_takes_object(object);
}

/**
 * Set intent for next move
 * @param request Intent
 */
void Player::set_intent(Request request)
{
	intent = request;
}
