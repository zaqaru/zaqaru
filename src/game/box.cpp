/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "box.hpp"

#include <QDebug>

#include <algorithm>

/**
 * Default constructor
 */
Box::Box()
	: lower(0.0, 0.0, 0.0), upper(1.0, 1.0, 1.0)
{
}

/**
 * Constructor
 * @param p1 Box corner
 * @param p2 Opposite box corner
 */
Box::Box(const Vec3 &p1, const Vec3 &p2)
{
	double xmin = std::min(p1.x, p2.x);
	double xmax = std::max(p1.x, p2.x);
	double ymin = std::min(p1.y, p2.y);
	double ymax = std::max(p1.y, p2.y);
	double zmin = std::min(p1.z, p2.z);
	double zmax = std::max(p1.z, p2.z);

	lower = Vec3(xmin, ymin, zmin);
	upper = Vec3(xmax, ymax, zmax);
}

/**
 * Indicates whether the bounding box intersects with another one
 */
bool Box::intersects(const Box &box) const
{
	if (lower.x > box.upper.x)
		return false;
	if (lower.y > box.upper.y)
		return false;
	if (lower.z > box.upper.z)
		return false;
	if (upper.x < box.lower.x)
		return false;
	if (upper.y < box.lower.y)
		return false;
	if (upper.z < box.lower.z)
		return false;

	return true;
}
