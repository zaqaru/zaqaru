/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "guard.hpp"

#include <QDebug>

/**
 * Constructor
 * @param root Root scene
 * @param absolute_position Guard absolute position in the scene
 */
Guard::Guard(Scene *root, const Vec3 &absolute_position)
	: Unit(root, absolute_position), ai(*scene)
{
	walk_speed = 0.0025;
	climb_speed = 0.0025;
	fall_speed = 0.0035;
}

/**
 * Whether the guard may be trapped by a destroyed block (always true)
 */
bool Guard::trappable() const
{
	return true; // TODO should be property rather than virtual function
}

/**
 * Update guard
 */
void Guard::update(gametime dt, const std::vector<Unit *> &obstacles)
{
	if (state == NORMAL) {
		// TODO dont't call this every frame
		ai.set_current(base, position);
		intent = ai.get_intent(position);
	}

	Unit::update(dt, obstacles);

	// TODO
	// if (state == NORMAL) ai.set_current(base, position);
	// set target?
}

/**
 * Handle guard action
 * @param dt Time step
 * @return true if time consumed
 */
bool Guard::check_action()
{
	return false;
}

/**
 * Check object presence
 */
void Guard::check_object_presence()
{
	// Nothing yet
}

/**
 * Initialize
 * @param player target
 */
void Guard::initialize(const Unit *player)
{
	ai.set_current(base, position);
	set_target(player->get_base(), player->get_rpos());
}

/**
 * Set target
 * @param target_base Base element of target
 * @param target_position Relative position of target
 */
void Guard::set_target(const Element *target_base, const Vec3 &target_position)
{
	if (target_base)
		ai.set_target(target_base, target_position);
	else
		qDebug() << "Assertion failed" << __FILE__ << __LINE__;
}

/**
 * Set intent: triggers AI path finding.
 */
void Guard::set_intent(Request request)
{
	Q_ASSERT(request == NONE);
	if (state != NORMAL) {
		ai.set_current(base, position + vec3::Y1);
		intent = ai.get_intent(position + vec3::Y1);
	} else {
		qDebug() << "Unexpected:" << __FILE__ << __func__;
	}
}

/**
 * Get AI path
 * @return list of vertices (strip)
 */
std::vector<Vec3> Guard::get_path() const
{
	return ai.get_path();
}

/**
 * Get AI graph
 * @return list of vertices (segments)
 */
std::vector<Vec3> Guard::get_graph() const
{
	return ai.get_graph();
}
