/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <string>

class Settings
{
public:
	static void save();

	static bool read_fullscreen();
	static std::string read_level_file();
	static int read_level_number();
	static std::string read_output_file();

	static void write_fullscreen(bool);
	static void write_level_file(const std::string &);
	static void write_level_number(int);
	static void write_output_file(const std::string &);

private:
	static void check_status(const char *, const char *);
	static bool read_bool(const char *);
	static int read_integer(const char *);
	static std::string read_string(const char *);
	static void write_bool(const char *, bool);
	static void write_integer(const char *, int);
	static void write_string(const char *, const std::string &);
};

#endif // SETTINGS_HPP
