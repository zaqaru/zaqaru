/*
 * Zaqaru
 * Copyright © 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX_HPP
#define BOX_HPP

#include "vec.hpp"

/**
 * Axis-aligned bounding box
 */
class Box
{
public:
	Box();
	Box(const Vec3 &p1, const Vec3 &p2);

	bool intersects(const Box &box) const;

private:
	Vec3 lower;
	Vec3 upper;
};

#endif // BOX_HPP
