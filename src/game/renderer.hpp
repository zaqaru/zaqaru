/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RENDERER_H
#define RENDERER_H

#include "vec.hpp"

#include <SDL2/SDL_opengl.h>

#include <vector>

struct Scene;
struct SDL_Window;
struct TexturedForm;

class Renderer
{
public:
	Renderer (const Scene &);
	~Renderer();

	void initialize();
	void close();
	void swap_window();
	void change_size(int x, int y);
	void toggle_fullscreen();
	void render();

private:
	void create_default_textures(int width);
	void draw_textured_object(const TexturedForm &object,
				  const Vec3 &position, int angle) const;

	void render_ai_graph() const;
	void render_objects() const;
	void render_overlay() const;
	void render_scene() const;
	void render_textured_elements() const;
	void render_wireframe_elements() const;
	void setup_texture(unsigned int id, unsigned int width);
	void throw_sdl_exception(const char *message);

	const Scene &scene;
	SDL_Window *sdl_window = nullptr;
	bool viewport_ok = true;
	int viewport_width;
	int viewport_height;
	int viewport_offset_x;
	int viewport_offset_y;

	bool display_frame = true;
	bool display_dashboard = false;

	std::vector<GLuint> textures;
	std::vector<std::vector<GLfloat>> bitmaps;
};

#endif // RENDERER_H
