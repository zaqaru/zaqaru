/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCENE_HPP
#define SCENE_HPP

#include "camera.hpp"
#include "defs.hpp"
#include "element.hpp"
#include "guard.hpp"
#include "player.hpp"

#include <QJsonObject>

#include <list>
#include <memory>
#include <vector>

/**
 * Describes the contents of the scene. It contains:
 * - elements (centered on integer coordinates)
 * - objects (standing above elements)
 * - the camera
 * - the player
 * - guards
 */
class Scene
{
public:
	enum Status {
		PLAY, END_WIN, END_FAIL,
	};

	enum ExitStyle {
		TOP_ROW, TOP_OF_TOP_ROW, EXIT_ELEMENT,
	};

	Scene();
	~Scene();

	void add_camera(int id, const Vec3 &position, const Vec3 &target,
			double weight = 0.0);
	void add_element(Element *);
	void add_element(Element::Type type, const Vec3i &position);
	void add_element(Element::Type type, int x, int y, int z = 0);
	void add_object(Object::Type type, const Vec3i &position);
	void add_light(const Vec3 &position);

	void compute_limits();
	Unit::Request compute_request(UserCommand command);
	void clear();

	Element *find_element_below(const Vec3i &p) const;
	Vec3i find_obstacle_candidate(const Element *, double y,
				      Axis, Direction) const;
	Vec3 camera_position() const;
	Vec3 camera_target() const;
	Element *element_at(const Vec3i p) const;
	std::vector<Element *> get_elements() const;
	std::vector<Vec3> get_lights() const;
	const Unit &get_player() const;
	std::vector<Guard *> get_guards() const;
	Element *neighbour(const Element *, const Vec3i &) const;
	Element *lower_neighbour(const Element *element, const Vec3i &r) const;

	void check_exit();
	void on_unit_base_change(Unit *);
	void player_takes_object(Object);
	void remove_at(const Vec3i &p);
	void remove_element(Element *element);

	void trap_unit(const Element *closing, const Unit *unit);

	void setup_test_level();
	void unlock();
	Status update(unsigned int dt, UserCommand command);

	// Serialization
	void read(const QJsonObject &json);
	void write(QJsonObject &scene_json) const;

private:
	void fill_elements(Element::Type type, Vec3i u, Vec3i v,
			   int camera_id, int angle,
			   const std::set<Element::Property> &properties);
	void fill_elements(Element::Type type, Vec3i u, Vec3i v,
			   int camera_id = 0, int angle = 0);
	void add_diagonal(Element::Type type, Vec3i u, Vec3i v,
			   int camera_id, int angle,
			   const std::set<Element::Property> &properties);
	void add_diagonal(Element::Type type, Vec3i u, Vec3i v,
			   int camera_id = 0, int angle = 0);

	void initialize();
	void remove_guard(const Unit *guard);
	void respawn_guard();

	std::map<Vec3i, Element *> elements;
	std::vector<Guard *> guards;
	std::vector<Element *> respawn_spots;
	std::vector<Vec3> lights;

	Player player;
	Camera camera;
	bool initialized = false;
	int upper_row = 0;
	int lower_row = 0;
	Status status = PLAY;
	ExitStyle exit_style = TOP_ROW;
	int goal_score = 0;
	int coin_score = 0;
	unsigned int guards_number = 0;
};

#endif // SCENE_HPP
