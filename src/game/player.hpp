/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "unit.hpp"

class Player : public Unit
{
public:
	Player(Scene *scene, const Vec3 &absolute_position = Vec3());

	bool trappable() const override;
	void update(gametime dt, UserCommand command,
		    const std::vector<Unit *> &obstacles);

protected:
	void check_object_presence() override;
	void set_intent(Request = NONE) override;

private:
	void initialize();

	bool check_action() override;
};

#endif // PLAYER_HPP
