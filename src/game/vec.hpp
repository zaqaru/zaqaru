/*
 * Zaqaru
 * Copyright © 2018, 2019 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VEC_HPP
#define VEC_HPP

#include "defs.hpp"

#include <QJsonObject>

#include <string>

struct Vec3;

struct Vec2i
{
	int x = 0;
	int y = 0;

	Vec2i();
	Vec2i(int x, int y);
};

struct Vec3i
{
	int x = 0;
	int y = 0;
	int z = 0;

	Vec3i();
	Vec3i(int x, int y, int z);
	explicit Vec3i(const Vec2i &);
	explicit Vec3i(const Vec3 &);

	void clear();

	// Serialization
	void read(const QJsonObject &json);
	void write(QJsonObject &json) const;
};

struct Vec3
{
	double x = 0;
	double y = 0;
	double z = 0;

	Vec3();
	Vec3(double x, double y, double z);
	Vec3(double azimuth, double inclination);
	explicit Vec3(const Vec3i &v);

	void clear();
	void set_length(double);

	double dot(const Vec3 &) const;
	bool is_null() const;
	double length() const;
	double length2() const;
	Vec3 normalized() const;
	Vec3 rotated(int) const;

	// Serialization
	std::string to_string() const;
	void read(const QJsonObject &json);
	void write(QJsonObject &json) const;
};

Vec2i operator+(const Vec2i &, const Vec2i &);
bool operator<(const Vec2i &, const Vec2i &);

Vec3i operator+(const Vec3i &, const Vec3i &);
Vec3i operator-(const Vec3i &, const Vec3i &);
bool operator==(const Vec3i &, const Vec3i &);
bool operator<(const Vec3i &, const Vec3i &);

Vec3 operator+(const Vec3 &, const Vec3 &);
Vec3 operator+(const Vec3 &, const Vec3i &);
Vec3 operator-(const Vec3 &, const Vec3 &);
Vec3 operator-(const Vec3 &, const Vec3i &);
Vec3 operator*(const Vec3 &, unsigned int);
Vec3 operator*(const Vec3 &, double);
Vec3 operator/(const Vec3 &, double);

Vec3 create_vec3(Axis axis, Direction direction);

namespace vec3 {

const Vec3 X1 { 1.0, 0.0, 0.0 };
const Vec3 Y1 { 0.0, 1.0, 0.0 };
const Vec3 Z1 { 0.0, 0.0, 1.0 };

}

namespace vec3i {

const Vec3i X1 { 1, 0, 0 };
const Vec3i Y1 { 0, 1, 0 };
const Vec3i Z1 { 0, 0, 1 };

}

#endif // VEC_HPP
